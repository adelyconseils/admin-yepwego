import React, { Component, Fragment } from "react";
//UI components
import { Colxx, Separator } from "components/CustomBootstrap";
import BreadcrumbContainer from "components/BreadcrumbContainer";
import { Row, Card, CardBody, CardTitle } from "reactstrap";
import IconCardsCarousel from "components/IconCardsCarousel";
import ChartAccountTypes from "components/ChartAccountTypes";
import ChartDevicesUsage from "components/ChartDevicesUsage";
// import AppVisitsChartCard from "components/AppVisitsChartCard";
import ReportReason from "components/ReportReason";
import GlideComponent from "components/GlideComponent";
import TopCategoryItems from "components/TopCategoryItems";
import CarrdStatictics from "components/CardStatictics";
import CardUsersBlocked from "components/CardUsersBlocked";
//ChartDeviceUsage
import { connect } from "react-redux";
//Api
import { getAllReportReason, getAllReportUser } from "api/report";
import { getAllUsers } from "api/user";
import { getUserBlocked } from "api/blockedUser";
import {
	getAllCategories,
	getCountActivitiesWithCategoryID,
	getCoverCategoryID
} from "api/category";
import { countModel, countModelWithDateOperation } from "api/count";
import { countActivitiesWithDate } from "api/activities";
import moment from "moment";
import { ThemeColors } from "components/ChartAccountTypes/ThemeColors";
const colors = ThemeColors();
let rootStyle = getComputedStyle(document.body);
const pathColor = rootStyle.getPropertyValue("--theme-color-2").trim();
class Dashboards extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			users: [],
			activities: [],
			totalUsers: {
				title: "Total d'utilisateurs",
				icon: "simple-icon-user",
				value: 0
			},
			totalMales: {
				title: "Total Masculin",
				icon: "simple-icon-symbol-male",
				value: 0
			},
			totalFemales: {
				title: "Total Féminin",
				icon: "simple-icon-symbol-female",
				value: 0
			},
			//acquis
			newUsers: {
				title: "Nouveaux utilisateurs",
				icon: "simple-icon-user-follow",
				value: 0
			},
			deviseUsageChartData: {
				labels: ["Android", "iOS"],
				datasets: [
					{
						label: "",
						borderColor: [colors.themeColor3, colors.themeColor2],
						backgroundColor: [colors.themeColor3_10, colors.themeColor2_10],
						borderWidth: 2,
						data: [45, 18]
					}
				]
			},
			socialMediaProvider: [0, 0, 0],
			topTreeCategories: [],
			userConnection: [],
			isLoading: true,
			reportReason: [],
			reportUser: [],
			blockedUsers: [],
			UsersWhoBlockedId: [],
			usersToBlockedId: [],
			allReportsReason: [],
			categoriesWithCount: [],
			countModel: [{}],
			activitiesModel: []
		};
	}
	/**
	 * Get user data
	 */
	usersData() {
		const { id: accessToken } = this.props.currentUser;
		/**
		 * Get all users
		 * @param: user accessToken
		 */
		getAllUsers({ accessToken })
			.then(users => {
				var now = moment();
				/*** Filter the new user in current week	 */
				var newUsers = users.filter(
					item => now.isoWeek() === moment(item.createdAt).isoWeek()
				);
				/*** Filter the male user */

				var totaleMales = users.filter(item =>
					item.Profile ? item.Profile.isMale : null
				);
				/***  filter Google accounts */
				var googleArray = users.filter(
					item => item.email.indexOf("google") > -1
				);
				/***  filter Facebook accounts */
				let facebookArray = users.filter(item => item.email.indexOf("fb") > -1);
				var socialMediaArray = [];
				/***  Filter Yepwego Form  accounts */
				let countAccount = Math.round(
					((users.length - (googleArray.length + facebookArray.length)) * 100) /
						users.length
				);
				/***  Purcent  */
				let googlePurcent = Math.round(
					(googleArray.length * 100) / users.length
				);
				let fbPurcent = Math.round((facebookArray.length * 100) / users.length);
				socialMediaArray.push(googlePurcent);
				socialMediaArray.push(fbPurcent);
				socialMediaArray.push(countAccount);

				const userConnection = users.filter(
					item => now.isoWeek() === moment(item.lastLogin).isoWeek()
				);
				/*** State */
				this.setState(prevState => ({
					totalUsers: {
						...prevState.totalUsers,
						value: users.length
					},
					newUsers: {
						...prevState.newUsers,
						value: newUsers.length
					},
					totalMales: {
						...prevState.totalMales,
						value: totaleMales.length
					},
					totalFemales: {
						...prevState.totalFemales,
						value: users.length - totaleMales.length
					},
					socialMediaProvider: socialMediaArray,
					users: users,
					userConnection
				}));
				this.usersDataForCards();
			})
			.catch(error => console.log(error));
	}
	/**
	 * Build the table of users data for card user
	 */
	usersDataForCards() {
		var newData = [];
		newData.push(this.state.totalUsers);
		newData.push(this.state.totalMales);
		newData.push(this.state.totalFemales);
		newData.push(this.state.newUsers);
		this.setState({ data: newData });
	}
	/**
	 *  1. Get users blocked
	 *  2. Groups each user blocked by who
	 * @param {String} accessToken
	 */
	getBlockedUser(accessToken) {
		getUserBlocked({ accessToken })
			.then(blockedUsers => {
				//console.log(blockedUsers);
				const groupedBlockedUsers = blockedUsers.reduce(
					(
						catsSoFar,
						{
							userToBlockId = [],
							userId,
							fkBlockeduserUserrel,
							fkBlockeduserUsertoblockrel
						}
					) => {
						if (!catsSoFar[userToBlockId]) catsSoFar[userToBlockId] = [];
						catsSoFar[userToBlockId].push({
							userToBlockId,
							userId,
							fkBlockeduserUserrel,
							fkBlockeduserUsertoblockrel
						});
						return catsSoFar;
					},
					{}
				);
				// const usersToBlockedId = Object.values(groupedBlockedUsers);
				// console.log("===", usersToBlockedId);
				this.setState({
					blockedUsers,
					usersToBlockedId: groupedBlockedUsers
				});
			})
			.catch(error => console.log("error => ", error));
	}
	/**
	 *  Get report
	 */
	reportData() {
		var reasons = this.state.reportReason;
		var reports = this.state.reportUser;
		var arrayReports = [];
		reasons.forEach(reason => {
			const d = reports.filter(report => {
				return report.reasonId === reason.reasonId;
			});
			arrayReports.push(d);
		});
		this.setState({ allReportsReason: arrayReports });
	}
	/**
	 *  Get reaport
	 *  1. get reason
	 *  2. get reaport
	 * @param {String} accessToken
	 */
	getAllReportUsers(accessToken) {
		/*** Get reason @param: accessToken */
		getAllReportReason({ accessToken }).then(reasons => {
			var reasonsWithOther = reasons;
			reasonsWithOther.push({
				reasonId: null,
				title: "Autre",
				title_en: "Other"
			});
			this.setState({ reportReason: reasons });
			/*** Get All Report @param: accessToken */
			getAllReportUser({ accessToken })
				.then(reports => {
					//console.log("reports ===> ", reports);
					/*** Group the report with his reason */
					var arrayReports = [];
					reasons.forEach((reason, index) => {
						const d = reports.filter(report => {
							return report.reasonId === reason.reasonId;
						});
						arrayReports[index] = d.length;
					});
					arrayReports.push(reports.length);
					this.setState({
						allReportsReason: arrayReports,
						reportUser: reports
					});
				})
				.catch(error => console.log(error));
		});
	}
	/**
	 * Get  activities count with date
	 * @param {String} accessToken
	 */
	async getAllActivities(accessToken) {
		let nowDate = new Date();
		let sum = "";
		let quaryOperationToDo = `?[where][startAt][gt]=${nowDate.toJSON()}`;
		let quaryOperationTerminated = `?[where][startAt][lt]=${nowDate.toJSON()}`;
		let quaryOperationInProgress = `?[where][startAt]=${nowDate.toJSON()}`;
		var arrayData = [];
		try {
			let totalActivitiesCreated = await countActivitiesWithDate(
				accessToken,
				sum
			);
			totalActivitiesCreated["title"] = "Activités créées";
			let toDoActivities = await countActivitiesWithDate(
				accessToken,
				quaryOperationToDo
			);
			toDoActivities["title"] = "Activités à faire";
			let completedActivities = await countActivitiesWithDate(
				accessToken,
				quaryOperationTerminated
			);
			completedActivities["title"] = "Activités terminés";
			//	console.log("completedActivities", completedActivities);
			let currentActivities = await countActivitiesWithDate(
				accessToken,
				quaryOperationInProgress
			);
			currentActivities["title"] = "Activités en cours";
			arrayData.push(
				totalActivitiesCreated,
				currentActivities,
				completedActivities,
				toDoActivities
			);
			this.setState({ activitiesModel: arrayData });
		} catch (error) {
			throw error;
		}
	}
	/**
	 * get number of payment with date
	 * @param {string} accessToken
	 */
	async countPaymentWithDate(accessToken) {
		try {
			let nowDate = new Date();
			let operation = `?[where][createdAt]=${nowDate.toJSON()}`;
			let payment = await countModelWithDateOperation(
				accessToken,
				"payment",
				operation
			);
			console.log("payment == ", payment);
		} catch (error) {
			throw error;
		}
	}
	/**
	 * 1.Get All categories
	 * 2.Get Count Activities for each CategoryID
	 * 3.Get Cover name  for each category
	 * @param {string} accessToken user token
	 */
	getAllCategoriesList(accessToken) {
		try {
			getAllCategories(accessToken).then(categories => {
				var categoriesWithCount = categories;
				categories.map(async (category, index) => {
					let currentCategory = category;
					/** Get Count Activities for each CategoryID */
					getCountActivitiesWithCategoryID(
						accessToken,
						currentCategory.categoryId
					).then(count => {
						/*** Get Cover name  for each category */
						getCoverCategoryID(accessToken, currentCategory.categoryId).then(
							cover => {
								currentCategory["activiriesCount"] = count.count;
								currentCategory["cover"] = cover.name;
								categoriesWithCount[index] = currentCategory;
							}
						);
					});
				});
				this.setState({ categoriesWithCount, isLoading: false });
			});
		} catch (error) {
			throw error;
		}
	}
	/***
	 *
	 */
	async getCountModel(accessToken) {
		try {
			var modelData = [];
			let listPayment = await countModel(accessToken, "payment");
			listPayment["title"] = "Transactions de paiement";
			let listConversation = await countModel(accessToken, "conversation");
			listConversation["title"] = "Conversations";
			let listConversationMessage = await countModel(accessToken, "messages");
			listConversationMessage["title"] = "Messages";
			let listFollowRequest = await countModel(accessToken, "followRequest");
			listFollowRequest["title"] = "Demandes de suivi";
			let listInvitationRequest = await countModel(
				accessToken,
				"invitationRequest"
			);
			listInvitationRequest["title"] = "Demandes d'invitation";
			let participationRequests = await countModel(
				accessToken,
				"ParticipationRequests"
			);
			participationRequests["title"] = "Demandes de participation";
			modelData.push(
				listPayment,
				listConversation,
				listConversationMessage,
				listFollowRequest,
				listInvitationRequest,
				participationRequests
			);
			this.setState({ countModel: modelData });
		} catch (error) {
			throw error;
		}
	}
	/** ComponentDidMount */
	componentDidMount() {
		const { id: accessToken } = this.props.currentUser;
		this.usersData();
		this.usersDataForCards();
		this.getAllReportUsers(accessToken);
		this.getBlockedUser(accessToken);
		this.reportData();
		this.getAllActivities(accessToken);
		this.getAllCategoriesList(accessToken);
		this.getCountModel(accessToken);
		this.countPaymentWithDate(accessToken);
	}
	/** Render component of top categpry  */
	cardTopCategory = () => {
		const activitiesArray = this.state.categoriesWithCount;
		var arraySorted = activitiesArray.sort(function(a, b) {
			return b.activiriesCount - a.activiriesCount;
		});
		//console.log("array Sorted ", arraySorted);
		return arraySorted.map((element, index) => {
			if (index < 3) {
				return (
					<TopCategoryItems
						key={"item" + index}
						data={element}
						index={index + 1}
					/>
				);
			}
		});
	};
	cardModel = () => {
		let data = this.state.countModel;
		return data.map((model, index) => {
			return (
				<CarrdStatictics
					key={"cardModel" + index}
					model={model}
					percent={false}
					pathColor={pathColor}
				/>
			);
		});
	};
	cardActivitiesStatics = () => {
		let data = this.state.activitiesModel;
		console.log("this.state.modelData", this.state.activitiesModel);
		return data.map((model, index) => {
			return (
				<CarrdStatictics
					key={"cardActivitiesStatics" + index}
					model={model}
					percent={true}
					pathColor={pathColor}
					total={data[0].count}
				/>
			);
		});
	};
	render() {
		const {
			data,
			isLoading,
			socialMediaProvider,
			reportReason,
			allReportsReason,
			deviseUsageChartData,
			blockedUsers
		} = this.state;
		const labelCompteName = ["% Google", "% Facebook", "% Yepwego"];
		return (
			<Fragment>
				<Row>
					<div className="disable-text-selection full-width">
						<Colxx xxs="12">
							<div className="mb-2 d-flex justify-content-between">
								<div>
									<BreadcrumbContainer
										heading={"Tableau de bord"}
										match={this.props.match}
									/>
								</div>
							</div>
							<Separator className="mb-5" />
						</Colxx>
					</div>
				</Row>
				{isLoading ? (
					<div className="loading" />
				) : (
					<>
						<Row>
							<Colxx lg="12" md="12" className="mb-4">
								<IconCardsCarousel data={data} />
							</Colxx>
						</Row>

						<Row>
							<Colxx lg="6" md="12" className="mb-4">
								<ChartAccountTypes
									chartClass="dashboard-donut-chart"
									label={labelCompteName}
									data={socialMediaProvider}
								/>
							</Colxx>
							<Colxx lg="6" md="12" className="mb-4">
								<ChartDevicesUsage
									chartClass="dashboard-donut-chart mb-4"
									deviseUsageChartData={deviseUsageChartData}
								/>
							</Colxx>
						</Row>
						<Row>
							<Colxx lg="6" md="12" className="mb-4">
								<ReportReason
									data={reportReason}
									allReportsReason={allReportsReason}
								/>
							</Colxx>
							<Colxx lg="6" md="12" className="mb-4">
								<CardUsersBlocked blockedUsers={blockedUsers} />
							</Colxx>
						</Row>
						<Row>
							<Colxx lg="12" md="12" className="mb-4">
								<Card className="dashboard-top-rated h-100 mb-4">
									<CardBody>
										<CardTitle>
											<p className="font-weight-bold">Top Catégories</p>
										</CardTitle>
										<Row>
											<Colxx lg="6" md="12" className="mb-4">
												<GlideComponent
													settings={{
														gap: 5,
														perView: 1,
														type: "carousel",
														hideNav: true
													}}
												>
													<div
														key={"GlideComponent" + 0}
														className="d-flex flex-row mb-3 ml-3"
													>
														{this.cardTopCategory()}
													</div>
												</GlideComponent>
											</Colxx>
											<Colxx lg="6" md="12" className="mb-4">
												<Row>{this.cardActivitiesStatics()}</Row>
											</Colxx>
										</Row>
									</CardBody>
								</Card>
							</Colxx>
						</Row>
						<Row>{this.cardModel()}</Row>

						{/* <Row>
							<Colxx lg="6" md="12" className="mb-4">
								<AppVisitsChartCard userConnection={userConnection} />
							</Colxx>
						</Row> */}
					</>
				)}
			</Fragment>
		);
	}
}
const mapStateToProps = ({ auth }) => {
	const { user } = auth;
	return { currentUser: user };
};

export default connect(
	mapStateToProps,
	null
)(Dashboards);
