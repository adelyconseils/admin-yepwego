import React, { Component, Fragment } from "react";
//Ux
import {
	Row,
	Nav,
	NavItem,
	TabContent,
	TabPane,
	Table,
	Card,
	CardBody,
	CardSubtitle,
	CardText
} from "reactstrap";
import { NavLink } from "react-router-dom";
import classnames from "classnames";

//UI components
import { Colxx } from "components/CustomBootstrap";
import BreadcrumbContainer from "components/BreadcrumbContainer";
import Imagecard from "components/DashbordCard/ImageCard";
//API
import { getAllPaymentTransactions } from "api/payment";
//Redux
import { connect } from "react-redux";
//moment
import moment from "moment";
//user placeholder
import logo from "assets/img/user-13-icon-256.png";
class PaymentUI extends Component {
	constructor(props) {
		super(props);
		this.toggleTab = this.toggleTab.bind(this);
		this.state = {
			activeFirstTab: "1" /*initialize tab index to 1*/,
			isLoading: true,
			paymentTranstctionsList: [],
			groupedPaymentUser: []
		};
	}
	/**
	 * Toggle switch the nav tab
	 * @param {String} tab index of tab
	 */
	toggleTab(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeFirstTab: tab
			});
		}
	}
	/**
	 * Get all payment transactions
	 * @param {String} accessToken user token
	 */
	async getAllPaymentTransactions(accessToken) {
		try {
			let result = await getAllPaymentTransactions(accessToken);
			console.log("payment array == ", result);
			const groupedPaymentUser = result.reduce(
				(catsSoFar, { fkPaymentProfilerel = [], userId, paymentId }) => {
					if (!catsSoFar[userId]) catsSoFar[userId] = [];
					catsSoFar[userId].push({
						fkPaymentProfilerel,
						userId,
						paymentId
					});
					return catsSoFar;
				},
				{}
			);
			console.log("--------------");
			console.log(Object.values(Object.values(groupedPaymentUser)));
			console.log("--------------");
			this.setState({
				paymentTranstctionsList: result,
				groupedPaymentUser: groupedPaymentUser,
				isLoading: false
			});
		} catch (error) {
			throw error;
		}
	}
	/**
	 * render transaction table
	 */
	dateCard = () => {
		let paymentArray = this.state.paymentTranstctionsList;
		var arraySorted = paymentArray.sort(function(a, b) {
			return new Date(b.createdAt) - new Date(a.createdAt);
		});
		return (
			<Table striped>
				<thead>
					<tr>
						<th>#</th>
						<th>Date de transaction</th>
						<th>Utilisateur</th>
					</tr>
				</thead>
				<tbody>
					{arraySorted.map((element, index) => (
						<tr key={index}>
							<th scope="row">{element.paymentId}</th>
							<td>
								{moment(moment(element.createdAt)).format("DD-MM-YYYY à HH:MM")}
							</td>
							<td>
								{(element.fkPaymentProfilerel.firstName != null
									? element.fkPaymentProfilerel.firstName
									: "") +
									" " +
									(element.fkPaymentProfilerel.lastName != null
										? element.fkPaymentProfilerel.lastName
										: "")}
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		);
	};

	/**
	 * render user card
	 */
	usersCard = () => {
		let paymentArray = this.state.groupedPaymentUser;
		var arraySorted = Object.values(paymentArray).sort(function(a, b) {
			return b.length - a.length;
		});
		return Object.values(arraySorted).map((element, index) => (
			<Colxx md="6" sm="6" lg="4" xxs="12" key={index}>
				<Card className="d-flex flex-row mb-4" style={{ borderRadius: "10px" }}>
					<NavLink to="#" className="d-flex">
						<Imagecard
							alt={""}
							src={
								element[0].fkPaymentProfilerel.Avatar ||
								element[0].fkPaymentProfilerel.providerAvatarURL
									? element[0].fkPaymentProfilerel.Avatar
										? process.env.REACT_APP_URL_API +
										  "/storage/profile/download/" +
										  element[0].fkPaymentProfilerel.Avatar.name
										: element[0].fkPaymentProfilerel.providerAvatarURL
									: logo
							}
							userCard={true}
							text={
								(element[0].fkPaymentProfilerel.firstName != null
									? element[0].fkPaymentProfilerel.firstName
									: "") +
								" " +
								(element[0].fkPaymentProfilerel.lastName != null
									? element[0].fkPaymentProfilerel.lastName
									: "")
							}
							style={{ minWidth: 60, minHeight: 60 }}
							className={`img-thumbnail list-thumbnail align-self-center m-4 small rounded`}
						/>
					</NavLink>
					<div className=" d-flex flex-grow-1 min-width-zero">
						<CardBody className=" pl-0 align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero">
							<div className="min-width-zero">
								<NavLink to="/app/ui/cards">
									<CardSubtitle className="truncate mb-1 font-weight-bold">
										{(element[0].fkPaymentProfilerel.firstName != null
											? element[0].fkPaymentProfilerel.firstName
											: "") +
											" " +
											(element[0].fkPaymentProfilerel.lastName != null
												? element[0].fkPaymentProfilerel.lastName
												: "")}
									</CardSubtitle>
								</NavLink>
								<CardText className="text-muted text-small mb-2">
									{element.length} transactions
								</CardText>
							</div>
						</CardBody>
					</div>
				</Card>
			</Colxx>
		));
	};
	componentDidMount() {
		const { id: accessToken } = this.props.currentUser;
		console.log("payment UI = ", accessToken);
		this.getAllPaymentTransactions(accessToken);
	}
	render() {
		const { isLoading } = this.state;
		return (
			<Fragment>
				<Row>
					<div className="disable-text-selection full-width">
						<Colxx xxs="12">
							<div className="mb-2 d-flex justify-content-between">
								<div>
									<BreadcrumbContainer
										heading={"Transaction de paiement"}
										match={this.props.match}
									/>
								</div>
							</div>
							<Nav tabs className="separator-tabs ml-0 mb-5">
								<NavItem>
									<NavLink
										className={classnames({
											active: this.state.activeFirstTab === "1",
											"nav-link": true
										})}
										onClick={() => {
											this.toggleTab("1");
										}}
										to="#"
									>
										{"Date"}
									</NavLink>
								</NavItem>
								<NavItem>
									<NavLink
										className={classnames({
											active: this.state.activeFirstTab === "2",
											"nav-link": true
										})}
										onClick={() => {
											this.toggleTab("2");
										}}
										to="#"
									>
										{"Utilisateurs"}
									</NavLink>
								</NavItem>
							</Nav>
						</Colxx>
					</div>
				</Row>
				{isLoading ? (
					<div className="loading" />
				) : (
					<TabContent activeTab={this.state.activeFirstTab}>
						<TabPane tabId="1">
							<Row>{this.dateCard()}</Row>
						</TabPane>
						<TabPane tabId="2">
							<Row>{this.usersCard()}</Row>
						</TabPane>
					</TabContent>
				)}
			</Fragment>
		);
	}
}

const mapStateToProps = ({ auth }) => {
	const { user } = auth;
	return { currentUser: user };
};

export default connect(
	mapStateToProps,
	null
)(PaymentUI);
