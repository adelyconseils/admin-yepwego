import React, { Component, Fragment } from "react";
import { Row, Button } from "reactstrap";
//custom Component
import "react-confirm-alert/src/react-confirm-alert.css";
import ThumbListView from "./ThumbListView";
import ListPageHeading from "components/ListPageHeading";
import Pagination from "components/Pagination";
import DetailUserModal from "components/DetailUserModal";
import { Colxx } from "components/CustomBootstrap";
//Api
import {
	getAllProfiles,
	getAllProfilesWithoutlimit,
	UpdateUserToken
} from "api/profile";

import ReactExport from "react-export-excel";
//Redux
import { connect } from "react-redux";
import { handleUpdateTokenAsync } from "redux/actions";
//Escel
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
function collect(props) {
	return { data: props.data };
}
class UsersList extends Component {
	constructor(props) {
		super(props);
		this.updateCurrentUserToken = this.updateCurrentUserToken.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.mouseTrap = require("mousetrap");
		this.state = {
			isLoading: true,
			utilisateurs: [],
			selectedOrderOption: { column: "createdAt", label: "Date de création" },
			dropdownSplitOpen: false,
			modalOpen: false,
			currentPage: 1,
			totalItemCount: 1,
			totalPage: 25,
			search: "",
			selectedItems: [],
			lastChecked: null,
			displayMode: "thumblist",
			selectedPageSize: 10,
			orderOptions: [
				{ column: "firstName", label: "Nom" },
				{ column: "createdAt", label: "Date de création" },
				{ column: "google", label: "Google" },
				{ column: "fb", label: "Facebook" }
			],
			userAvatarId: [],
			userSelected: null,
			detailUser: {},
			tokensToAdd: 0,
			isOpenCollapse: false,
			profiles: [],
			isLoadingTokenButton: "default"
		};
		this.showDetailUser = this.showDetailUser.bind(this);
		this.toggleModal = this.toggleModal.bind(this);
	}

	/**
	 * component mount
	 */
	componentDidMount() {
		this.dataListRender();
		this.getAllProfilesWithoutlimitt();
		this.mouseTrap.bind(["ctrl+a", "command+a"], () =>
			this.handleChangeSelectAll(false)
		);
		this.mouseTrap.bind(["ctrl+d", "command+d"], () => {
			this.setState({
				selectedItems: []
			});
			return false;
		});
	}
	/**
	 * unmount component
	 */
	componentWillUnmount() {
		this.mouseTrap.unbind("ctrl+a");
		this.mouseTrap.unbind("command+a");
		this.mouseTrap.unbind("ctrl+d");
		this.mouseTrap.unbind("command+d");
	}
	/**
	 * open/or close detail user modal
	 */
	toggleModal() {
		this.setState({
			modalOpen: !this.state.modalOpen
		});
	}

	/**
	 * handle Submit
	 * @param {event} event
	 * @param {event} errors
	 * @param {event} values
	 */
	handleSubmit(event, errors, values) {
		console.log(errors);
		console.log(values);
		this.setState({ isLoadingTokenButton: "processing" });
		values["minPropNumberProp"] = "";
		if (errors.length === 0) {
			setTimeout(() => {
				this.updateCurrentUserToken();
			}, 3000);
		} else if (errors.length !== 0) {
			this.setState({ isLoadingTokenButton: "fail" });
		}
	}
	/**
	 * Paginate responses
	 * @param {number} page page number
	 */
	onChangePage = page => {
		this.setState(
			{
				currentPage: page
			},
			() => this.dataListRender()
		);
	};
	/**
	 * Specifies how to sort the results
	 */
	changeOrderBy = column => {
		this.setState(
			{
				selectedOrderOption: this.state.orderOptions.find(
					x => x.column === column
				)
			},
			() => this.dataListRender()
		);
	};
	/**
	 * filter the data with word enter with a user
	 * @param {event} e
	 */
	onSearchKey = e => {
		if (e.key === "Enter") {
			this.setState(
				{
					search: e.target.value
					//.toLowerCase()
				},
				() => this.dataListRender()
			);
		}
	};
	/**
	 * Check user card
	 * @param {event} event
	 * @param {string} id
	 */
	onCheckItem = (event, id) => {
		if (
			event.target.tagName === "A" ||
			(event.target.parentElement && event.target.parentElement.tagName === "A")
		) {
			return true;
		}
		if (this.state.lastChecked === null) {
			this.setState({
				lastChecked: id
			});
		}
		let selectedItems = this.state.selectedItems;
		/* Select already selected item */
		if (selectedItems.includes(id)) {
			this.setState({
				selectedItems: [],
				categoryToEdit: null,
				lastChecked: null
			});
		} else {
			/* Select new item */
			selectedItems = [];
			selectedItems.push(id);
			this.setState({
				selectedItems
			});
		}

		if (event.shiftKey) {
			console.log("even.shiftKey");
			var items = this.state.items;
			var start = this.getIndex(id, items, "id");
			var end = this.getIndex(this.state.lastChecked, items, "id");
			items = items.slice(Math.min(start, end), Math.max(start, end) + 1);
			selectedItems.push(
				...items.map(item => {
					return item.categoryId;
				})
			);
			selectedItems = Array.from(new Set(selectedItems));
			this.setState({
				selectedItems
			});
		}
		document.activeElement.blur();
	};
	/**
	 * handle change select all item
	 * @param {bool} isToggle
	 */
	handleChangeSelectAll = isToggle => {
		if (this.state.selectedItems.length >= this.state.items.length) {
			if (isToggle) {
				this.setState({
					selectedItems: []
				});
			}
		} else {
			this.setState({
				selectedItems: this.state.items.map(x => x.categoryId)
			});
		}
		document.activeElement.blur();
		return false;
	};
	/**
	 * Get all profile
	 * @param {string} 	accessToken user token
	 * @param {number} currentPage paginate responses
	 * @param {number} records_per_page number of records per page
	 * @param {string} search key for search
	 * @param {string} orderOption specifies how to sort the results
	 */
	async dataListRender() {
		const {
			selectedPageSize,
			currentPage,
			selectedOrderOption,
			search
		} = this.state;
		const { id: accessToken } = this.props.currentUser;
		const records_per_page = selectedPageSize;
		const orderOption = selectedOrderOption.column;
		try {
			getAllProfiles({
				accessToken,
				currentPage,
				records_per_page,
				search,
				orderOption
			})
				.then(profiles => {
					console.log("profiles===> ", profiles);
					this.setState({
						userAvatarId: profiles,
						items: profiles,
						utilisateurs: profiles,
						isLoading: false
					});
				})
				.catch(error => console.log("error=>", error));
		} catch (err) {
			throw err;
		}
	}
	/**
	 * Get all profiles for exporting in excel file
	 */
	async getAllProfilesWithoutlimitt() {
		const { id: accessToken } = this.props.currentUser;
		try {
			var profiles = await getAllProfilesWithoutlimit({
				accessToken
			});
			this.setState({
				profiles: profiles
			});
		} catch (err) {
			throw err;
		}
	}
	/**
	 * Modify the input field
	 * @param {event} event
	 */
	setUserToken = event => {
		this.setState({
			tokensToAdd: event.target.value
		});
	};
	/*
	return new Promise((success, fail) => {
		setTimeout(() => {
		  success("Everything went right!");
		}, 2000);
	  });
	  */
	/**
	 * Update number of user tokens
	 */
	async updateCurrentUserToken() {
		let user = this.state.detailUser;
		let tokensToAdd = this.state.tokensToAdd;
		const { id: accessToken } = this.props.currentUser;
		try {
			if (tokensToAdd > 0) {
				let userdata = {
					userId: String(user.userId),
					token: Number(user.tokensCount) + Number(tokensToAdd)
				};
				let response = await this.props.handleUpdateTokenAsync(
					accessToken,
					userdata
				);
				console.log("response ", response);
				user["tokensCount"] = userdata.token;
				this.setState({
					isLoadingTokenButton: "success",
					detailUser: user,
					tokensToAdd: 0
				});
				setTimeout(() => {
					this.setState({
						isLoadingTokenButton: "default"
					});
				}, 2000);

				this.dataListRender();
			} else {
				this.setState({
					isLoadingTokenButton: 1,
					tokensToAdd: 0
				});
				setTimeout(() => {
					this.setState({
						isLoadingTokenButton: "default"
					});
				}, 2000);
			}
		} catch (error) {
			throw error;
		}
	}

	/**
	 * Show user detail
	 * @param {event} event
	 * @param {sring} id user id
	 */
	showDetailUser = (event, id) => {
		let usersArray = this.state.userAvatarId;
		let detailUser = usersArray.find(element => element.userId === id);

		const userOject = {
			Activities: detailUser.Activities.length,
			//Categories: detailUser.Categories.legth,
			Followers: detailUser.Followers.length,
			ParticipatedActivities: detailUser.ParticipatedActivities.length,
			BlockedProfile: detailUser.BlockedProfile.length,
			Avatar: detailUser.Avatar ? detailUser.Avatar.name : null,
			email:
				detailUser.User.email.indexOf("@fb") > -1 ||
				detailUser.User.email.indexOf("@google") > -1
					? detailUser.User.email.indexOf("@fb") > -1
						? "Facebook"
						: "Google"
					: detailUser.User.email,
			createdAt: detailUser.User.createdAt,
			lastLogin: detailUser.User.lastLogin,
			birthdate: detailUser.birthdate,
			countryCode: detailUser.countryCode,
			description: detailUser.description,
			firstName: detailUser.firstName,
			isMale: detailUser.isMale,
			lastName: detailUser.lastName,
			lastNotificationsSeenDate: detailUser.lastNotificationsSeenDate,
			providerAvatarURL: detailUser.providerAvatarURL,
			pseudo: detailUser.pseudo,
			sponsoredBy: detailUser.sponsoredBy,
			sponsorshipCode: detailUser.sponsorshipCode,
			tokensCount: detailUser.tokensCount,
			userId: detailUser.userId
		};

		this.setState({ detailUser: userOject });
		this.toggleModal();
	};
	/**
	 * Return users card
	 * @param {JSON} utilisateursObject array of users profiles
	 */
	userCards = utilisateursObject => {
		const activitiesArray = utilisateursObject;
		return activitiesArray.map((element, index) => (
			<Colxx xxs="12" key={`ThumbListView_${index}`} className="mb-3">
				<ThumbListView
					data={element}
					isSelect={this.state.selectedItems.includes(element.coverId)}
					collect={collect}
					onClickDetail={this.showDetailUser}
					onCheckItem={this.onCheckItem}
				/>
			</Colxx>
		));
	};
	/**
	 *
	 */
	render() {
		const {
			currentPage,
			items,
			displayMode,
			selectedPageSize,
			totalItemCount,
			selectedOrderOption,
			selectedItems,
			orderOptions,
			pageSizes,
			modalOpen,
			isLoading,
			userAvatarId,
			detailUser,
			profiles,
			isOpenCollapse,
			isLoadingTokenButton,
			tokensToAdd
		} = this.state;

		var a = profiles.map(element => {
			return {
				firstName: element.firstName,
				lastName: element.lastName,
				email: element.User.email
			};
		});
		const { match } = this.props;
		const startIndex = (currentPage - 1) * selectedPageSize;
		const endIndex = currentPage * selectedPageSize;
		return (
			<Fragment>
				<div className="disable-text-selection">
					<ListPageHeading
						heading="Liste des utilisateurs"
						displayMode={displayMode}
						// changeDisplayMode={this.changeDisplayMode}
						handleChangeSelectAll={this.handleChangeSelectAll}
						changeOrderBy={this.changeOrderBy}
						changePageSize={this.changePageSize}
						selectedPageSize={selectedPageSize}
						totalItemCount={totalItemCount}
						selectedOrderOption={selectedOrderOption}
						match={match}
						startIndex={startIndex}
						endIndex={endIndex}
						selectedItemsLength={selectedItems ? selectedItems.length : 0}
						itemsLength={items ? items.length : 0}
						onSearchKey={this.onSearchKey}
						orderOptions={orderOptions}
						pageSizes={pageSizes}
						toggleModal={this.toggleModal}
						buttonIsHidden={true}
					/>
					<DetailUserModal
						modalOpen={modalOpen}
						toggleModal={this.toggleModal}
						user={detailUser}
						isOpenCollapse={isOpenCollapse}
						handleSubmit={this.handleSubmit}
						setUserToken={this.setUserToken}
						isLoadingTokenButton={isLoadingTokenButton}
						tokensToAdd={tokensToAdd}
					/>
				</div>
				{isLoading ? (
					<div className="loading" />
				) : (
					<Row>
						{this.userCards(userAvatarId)}
						<Pagination
							currentPage={this.state.currentPage}
							totalPage={this.state.totalPage}
							onChangePage={i => this.onChangePage(i)}
						/>

						<ExcelFile
							element={
								<Button className="w-35" color="secondary" outline>
									Exporter en Excel
								</Button>
							}
						>
							<ExcelSheet data={a} name="userExcel">
								<ExcelColumn label="FirstName" value="firstName" />
								<ExcelColumn label="Last Name" value="lastName" />
								{/* <ExcelColumn label="E-mail" value="email" /> */}
								<ExcelColumn
									label="Email"
									value={col =>
										col.email.indexOf("@fb") > -1 ||
										col.email.indexOf("@google") > -1
											? col.email.indexOf("@fb") > -1
												? "Facebook"
												: "Google"
											: col.email
									}
								/>
							</ExcelSheet>
						</ExcelFile>
					</Row>
				)}
			</Fragment>
		);
	}
}
const mapStateToProps = ({ auth }) => {
	const { user } = auth;
	return { currentUser: user };
};

export default connect(
	mapStateToProps,
	{ handleUpdateTokenAsync }
)(UsersList);
