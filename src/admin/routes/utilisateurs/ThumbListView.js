import React from "react";
import { Card, CustomInput } from "reactstrap";
import classnames from "classnames";
import { ContextMenuTrigger } from "react-contextmenu";
import moment from "moment";
import { NavLink } from "react-router-dom";
import Imagecard from "components/DashbordCard/ImageCard";
//logo-whitee.png
import logo from "assets/img/user-13-icon-256.png";
const ThumbListView = ({
	data,
	isSelect,
	collect,
	onClickDetail,
	onCheckItem
}) => {
	return (
		<ContextMenuTrigger id="menu_id" data={data.userId} collect={collect}>
			<Card
				className={classnames("d-flex flex-row", {
					active: isSelect
				})}
				onClick={event => onClickDetail(event, data.userId)}
			>
				{/* <NavLink to={`?p=${""}`} className="d-flex"> */}
				<NavLink to={`?p=${data.userId}`} className="d-flex">
					<img
						alt={"indisponible"}
						src={
							data.Avatar || data.providerAvatarURL
								? data.Avatar
									? process.env.REACT_APP_URL_API +
									  "/storage/profile/download/" +
									  data.Avatar.name
									: data.providerAvatarURL
								: logo
						}
						onClick={onClickDetail}
						className="list-thumbnail responsive border-0 card-img-left"
						style={{
							height: 100,
							width: 150
						}}
					/>
				</NavLink>
				{/* </NavLink> */}
				<div className="pl-2 d-flex flex-grow-1 min-width-zero">
					<div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
						<p className="mb-1 text-muted text-semibold w-15 w-sm-100 ">
							{data.firstName} {data.lastName}
						</p>
						{/* <p className="mb-1 text-muted text-semibold w-15 w-sm-100">
							{data.User.email}
						</p> */}
						{/* <p className="mb-1 text-muted font-semibold w-15 w-sm-100">
							{"Membre depuis " +
								moment(moment(data.User.createdAt)).format("DD/MM/YYYY HH:MM")}
						</p> */}
						<p className="mb-1 text-muted text-semibold w-15 w-sm-100">
							{data.Followers.length + " Abonnées"}
						</p>
						<p className="mb-1 text-muted text-semibold w-15 w-sm-100">
							{data.Activities.length + " Activités Créée"}
						</p>
						<p className="mb-1 text-muted text-semibold w-15 w-sm-100">
							{"Participé a " +
								data.ParticipatedActivities.length +
								" activités"}
						</p>
						<p className="mb-1 text-muted text-semibold w-15 w-sm-100">
							{data.tokensCount + " Jetons"}
						</p>

						{/* <div className="w-15 w-sm-100">
							
						</div> */}
					</div>
					<div className="custom-control custom-checkbox color-info pl-1 align-self-center pr-4">
						{/* <CustomInput
							className="item-check mb-0 "
							type="checkbox"
							id={`check_${data.userId}`}
							checked={isSelect}
							onChange={() => {}}
							label=""
						/> */}
					</div>
				</div>
			</Card>
		</ContextMenuTrigger>
	);
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(ThumbListView);
