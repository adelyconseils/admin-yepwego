import React, { Component } from "react";
import { getSingleExpert, getGenderExpert, updateExpert } from "api/expert.js";
import { withRouter } from "react-router-dom";
import { handleInputError } from "util/Validation";
import { toast } from "react-toastify";
import Select from "react-select";
import { getDomainsList, getDomainsListByToken } from "../../../api/domains";

import {
	Card,
	CardBody,
	Form,
	FormGroup,
	InputGroup,
	Input,
	Row,
	Col,
	Label,
	CustomInput,
	FormFeedback,
	Button,
	Spinner
} from "reactstrap";
const createOption = (label: string) => ({
	label,
	value: label,
	key: label
});
const CustomOption = ({ innerProps, isDisabled }) =>
	!isDisabled ? (
		<div {...innerProps}>{/* your component internals */}</div>
	) : null;

class EditUser extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstName: "",
			LastName: "",
			email: "",
			expert: {},
			dataExpert: {},
			selectedGender: "",
			expertDomains: [],
			domainError: false,
			gender: {},
			isLoading: false,
			errors: [],
			isActivated: false,
			domains: [],
			selectedDomains: [],
			selected: [],
			isLoadingDomain: false,
			domain: ""
		};
	}

	isValidForm = event => {
		//Récupérer les states
		const { selectedGender, firstName, LastName, selectedDomains } = this.state;
		//Initialiser l'object erreur
		console.log("selectedDomains", selectedDomains);
		let errors = {};

		//Vérifier les champs

		if (firstName.length == 0) {
			errors.firsName = { message: "Votre nom est vide !!!" };
		}
		if (LastName.length < 2) {
			errors.LastName = { message: "Votre prénom est vide !!!" };
		}

		if (selectedGender.length == 0) {
			errors.selectedGender = { message: "Veuillez sélectionner votre genre" };
		}

		if (Object.entries(errors).length !== 0) {
			return this.setState({ errors }, () => false);
		}
		return true;
	};
	handelActivated = () => {
		this.setState({ isActivated: !this.state.isActivated });
	};

	handelFormElement = event =>
		this.setState({ [event.target.name]: event.target.value });
	componentDidMount() {
		const { currentUser } = this.props;
		const { Id } = this.props.match.params;
		getSingleExpert({ currentUser, Id })
			.then(response => {
				this.setState({
					...response[0].expert,
					selectedGender: response[0].expert.genderId,
					isActivated: response[0].expert.isActivated,
					selectedDomains: response.map(elem => elem.domaine),
					expertDomains: response.map(elem => {
						return {
							expertId: elem.expertId,
							domainId: elem.domainId,
							date: elem.date
						};
					})
				});
				getGenderExpert({ currentUser, Id }).then(gender => {
					this.setState({ gender: gender });
				});
			})
			.catch(err => {
				alert("userNotFound");
			});
		var activatedDomains = JSON.stringify({ where: { isActivated: true } });
		getDomainsListByToken({ currentUser, activatedDomains })
			.then(domains => this.setState({ domains, isLoadingDomain: false }))
			.catch(error => console.log(error));
	}

	handleUpdateUser() {
		const {
			firstName,
			LastName,
			selectedGender,
			isActivated,
			dataExpert,
			expertDomains
		} = this.state;
		const { Id } = this.props.match.params;

		const expert = {
			firstName: firstName,
			LastName: LastName,
			genderId: selectedGender,
			isActivated: isActivated
		};
		const { currentUser } = this.props;
		dataExpert.expertId = Id;
		dataExpert.expert = expert;
		dataExpert.expertDomains = expertDomains;

		updateExpert({ currentUser, dataExpert })
			.then(() => {
				this.setState({ isLoading: false, errors: [] });
				toast.success("🙃 Vos informations générales on bien été modifiés !");
			})
			.catch(({ error = {} }) => {
				this.setState({ isLoading: false, errors: [] });
				let { code = "API_ERROR", message } = error;
				toast.error(message);
			});
	}

	handleChange = (value, actionMeta) => {
		const { Id } = this.props.match.params;
		console.group("Inside handleChange");
		let today = new Date();
		let dd = String(today.getDate());
		let mm = String(today.getMonth() + 1); //January is 0!
		let yyyy = today.getFullYear();
		let hour =
			today.getHours() < 10 ? "0" + today.getHours() : today.getHours();
		let min =
			today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes();
		let seconds =
			today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds();

		today = yyyy + "/" + mm + "/" + dd + " " + hour + ":" + min + ":" + seconds;
		console.log(value);
		const newItems = !value
			? ""
			: value.map(item => {
					return {
						expertId: Id,
						domainId: item.id,
						date: today
					};
			  });
		console.log("newItems", newItems);
		this.setState({
			expertDomains: newItems,
			selectedDomains: value
		});

		console.log(
			"expertDomainsHandled",
			JSON.stringify(this.state.expertDomains)
		);
	};

	handleInputChange = inputValue => {
		this.setState({ inputValue });
		console.log("inputValue", inputValue);
	};

	handleKeyDown = event => {
		const { inputValue, value } = this.state;
		if (!inputValue) return;
		console.log("event.key:", event.key);
		switch (event.key) {
			case "Enter":
				const newValues = inputValue.split(",").map(x => createOption(x));
				this.setState({
					inputValue: "",
					value: [...value, ...newValues]
				});
				event.preventDefault();
				break;
			case "Tab":
				this.setState({
					inputValue: "",
					value: [...value, createOption(inputValue)]
				});
				event.preventDefault();
				break;
			default:
				console.log("Other events caught");
		}
	};

	// Submit form
	handleSubmit = event => {
		event.preventDefault();
		const { selectedDomains } = this.state;

		if (!selectedDomains || !selectedDomains[0]) {
			this.setState({ colorError: true });
		} else if (this.isValidForm() || selectedDomains) {
			//Send information Api
			this.setState({ isLoading: true }, () => {
				console.log("form is valid");
				this.handleUpdateUser();
				console.log("dataExpert", this.state.dataExpert);

				this.setState({ colorError: false });
			});
		}
	};

	render() {
		const { gender } = this.props;
		const {
			firstName,
			LastName,
			email,
			isLoading,
			isLoadingDomain,
			selectedGender,
			domains,
			selectedDomains,
			inputValue,
			errors,
			isActivated,
			colorError
		} = this.state;

		return (
			<>
				<Row>
					{isLoadingDomain ? (
						<div className="loading" />
					) : (
						<Card>
							<CardBody>
								<Form onSubmit={this.handleSubmit}>
									<FormGroup>
										<Row>
											<Col md="3" sm="3">
												<Label for="firstName">Nom</Label>
											</Col>
											<Col>
												<InputGroup>
													<Input
														onChange={this.handelFormElement}
														name="firstName"
														invalid={handleInputError(errors, "firsName")}
														value={firstName}
													></Input>
													<FormFeedback>
														{errors.firsName && errors.firsName.message}
													</FormFeedback>
												</InputGroup>
											</Col>
										</Row>
									</FormGroup>
									<FormGroup>
										<Row>
											<Col md="3">
												<Label for="LastName">Prénom</Label>
											</Col>
											<Col>
												<Input
													type="text"
													name="LastName"
													placeholder="Saisir le prénom"
													onChange={this.handelFormElement}
													invalid={handleInputError(errors, "LastName")}
													value={LastName}
												/>
												<FormFeedback>
													{errors.firsName && errors.firsName.message}
												</FormFeedback>
											</Col>
										</Row>
									</FormGroup>
									<FormGroup>
										<Row>
											<Col md="3">
												<Label for="LastName">Email</Label>
											</Col>
											<Col>
												<Input
													disabled
													type="text"
													name="email"
													placeholder="Saisir le prénom"
													//	onChange={this.handelFormElement}
													invalid={handleInputError(errors, "email")}
													value={email}
												/>
												<FormFeedback>
													{errors.LastName && errors.LastName.message}
												</FormFeedback>
											</Col>
										</Row>
									</FormGroup>

									<FormGroup>
										<Label for="email">Genre</Label>
										<div>
											{gender.map(gender => (
												<CustomInput
													checked={gender.id == selectedGender ? true : null}
													key={gender.id}
													type="radio"
													id={gender.id}
													name={"selectedGender"}
													label={gender.name}
													value={gender.id}
													invalid={handleInputError(errors, "selectedGender")}
													inline
													onChange={this.handelFormElement}
												/>
											))}
										</div>
										<FormFeedback>
											{errors.selectedGender && errors.selectedGender.message}
										</FormFeedback>
									</FormGroup>
									<FormGroup>
										<Label for="activity">Domaines d'activité</Label>
										<div>
											<Select
												inputValue={inputValue}
												isClearable={false}
												isMulti
												name="domains"
												getOptionLabel={domain => domain.name}
												getOptionValue={domain => domain.id}
												options={domains}
												onChange={this.handleChange}
												onInputChange={this.handleInputChange}
												onKeyDown={this.handleKeyDown}
												value={selectedDomains}
												inputProps={{
													onInvalid: e =>
														e.target.setCustomValidity(
															"Invalid Custom message"
														),
													onInput: e => e.target.setCustomValidity("")
												}}
												styles={{
													control: (provided, state) =>
														colorError
															? {
																	...provided,
																	boxShadow: "0 0 0 1px red !important",
																	borderColor: "red !important"
															  }
															: provided
												}}
												className="basic-multi-select"
												classNamePrefix="select"
												required
											/>
										</div>
										<FormFeedback>
											{errors.selectedDomains && errors.selectedDomains.message}
										</FormFeedback>
									</FormGroup>
									<FormGroup>
										<Row>
											<Col md="3">
												<Label for="LastName">Activé</Label>
											</Col>
											<Col>
												<CustomInput
													type="switch"
													name="isActivated"
													checked={isActivated}
													value={isActivated}
													onChange={this.handelActivated}
												/>
											</Col>
										</Row>
									</FormGroup>

									<div className="float-left">
										<Button
											color="light"
											outline
											className="float-right"
											type="button"
											onClick={this.clearForm}
										>
											Annuler
										</Button>
									</div>
									<div className="float-right">
										{isLoading ? (
											<Spinner color="secondary" />
										) : (
											<Button
												color="primary"
												outline
												className="float-right"
												type="submit"
												disabled={isLoading}
											>
												Enregistrer
											</Button>
										)}
									</div>
								</Form>
							</CardBody>
						</Card>
					)}
				</Row>
			</>
		);
	}
}

export default withRouter(EditUser);
