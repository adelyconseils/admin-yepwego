import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import TopNav from "./containers/TopNav";
import Sidebar from "./containers/TopNav";
import Login from "./login";
const AppMainAdminRoutes = props => {
	const { match, history } = props;
	return (
		<div id="app-container">
			<TopNav history={history} />
			<Sidebar />
			<main>
				<div className="container-fluid">
					<Switch>
						<Route path={`${match.url}/connexion`} component={Login} />
						<Redirect to="/error" />
					</Switch>
				</div>
			</main>
		</div>
	);
};

export default AppMainAdminRoutes;
