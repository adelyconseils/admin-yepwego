import moment from "moment";
export const toDateSting = date => {
	return date;
};

/**
 * Formater une date
 * @param {*} date String
 * @param {*} format String
 */
export const formaterDate = (date, format) => moment(date).format(format);
