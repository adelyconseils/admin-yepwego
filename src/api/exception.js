import { urlApi } from "constants/defaultValues";
export const getExceptionList = async ({ accessToken }) => {
	try {
		const response = await fetch(urlApi + `/Exceptions`, {
			method: "GET",
			headers: {
				Authorization: accessToken,
				"Content-Type": "application/json"
			}
		});
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};

export const deleteException = async ({ accessToken, id }) => {
	try {
		const response = await fetch(urlApi + "/Exceptions/" + id, {
			method: "DELETE",
			headers: {
				Authorization: accessToken,
				"Content-type": "application/json"
			}
		});
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};

export const insertException = async exception => {
	try {
		const response = await fetch(urlApi + `/Exceptions`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(exception)
		});
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};
