import { authExpert, authCustomer } from "./../firebase";
import Firebase from "firebase";

const linkAccountEmailWithSocialProvider = (email, password, profileType) => {
	return new Promise((resolve, reject) => {
		const user =
			profileType === "Experts"
				? authExpert.currentUser
				: authCustomer.currentUser;
		if (!user) {
			resolve("");
		}
		const credential = Firebase.auth.EmailAuthProvider.credential(
			email,
			password
		);
		user.linkAndRetrieveDataWithCredential(credential).then(
			function(usercred) {
				var user = usercred.user;
				resolve(user);
			},
			function(error) {
				reject(error);
			}
		);
	});
};

const signInWithGoogle = profileType => {
	return new Promise((resolve, reject) => {
		const provider = new Firebase.auth.GoogleAuthProvider();
		const auth = profileType === "Experts" ? authExpert : authCustomer;
		auth.languageCode = "fr";
		provider.setCustomParameters({
			login_hint: "expert"
		});
		auth
			.signInWithPopup(provider)
			.then(function(result) {
				// This gives you a Google Access Token. You can use it to access the Google API.
				var token = result.credential.accessToken;
				// The signed-in user info.
				var user = result.user;
				resolve({ token, user });
			})
			.catch(function(error) {
				// Handle Errors here.
				var errorCode = error.code;
				var errorMessage = error.message;
				// ...
				reject({ errorCode, errorMessage });
			});
	});
};
const getJwtTokenFirebase = profileType => {
	const auth = profileType === "Experts" ? authExpert : authCustomer;
	return auth.currentUser.getIdToken();
};

const singOutUserFromFirebase = profileType => {
	const auth = profileType === "Experts" ? authExpert : authCustomer;
	return auth.signOut();
};

const requestCodeValidation = async (phoneNumber, appVerifier, profileType) => {
	const auth = profileType === "Experts" ? authExpert : authCustomer;
	return await auth.signInWithPhoneNumber(phoneNumber, appVerifier);
};

/**
 * Vérifier si le code reçue est valide.
 * Si l'utilisateur est déja connecté avec un social Auth , il faut lier les comptes
 * Voir => https://firebase.google.com/docs/auth/web/account-linking
 * @param {*} confirmationResult
 * @param {*} code
 * @param {*} profileType
 */
const confirmPhoneCode = async (confirmationResult, code, profileType) => {
	console.log(profileType);
	try {
		const auth = profileType === "Experts" ? authExpert : authCustomer;
		const credential = Firebase.auth.PhoneAuthProvider.credential(
			confirmationResult,
			code
		);
		const prevUser =
			profileType === "Experts"
				? authExpert.currentUser
				: authCustomer.currentUser;
		const userCredential = await auth.signInWithCredential(credential);
		if (prevUser) {
			const user = userCredential.user;
			var currentUser = userCredential;
			// Merge prevUser and currentUser data stored in Firebase.
			// Note: How you handle this is specific to your application

			// After data is migrated delete the duplicate user
			await user.delete();
			await prevUser.linkWithCredential(credential);
			// Sign in with the newly linked credential
			await auth.signInWithCredential(credential);
		}
		return userCredential;
	} catch (error) {
		throw error;
	}
};

export {
	signInWithGoogle,
	linkAccountEmailWithSocialProvider,
	getJwtTokenFirebase,
	singOutUserFromFirebase,
	requestCodeValidation,
	confirmPhoneCode
};
