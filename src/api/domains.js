import { urlApi } from "constants/defaultValues";
const getDomainsListByToken = async ({
	accessToken,
	activatedDomains = ""
}) => {
	try {
		const response = await fetch(
			urlApi + `/Domaines?filter=` + encodeURIComponent(activatedDomains),
			{
				method: "GET",
				headers: {
					Authorization: accessToken,
					"Content-Type": "application/json"
				}
			}
		);
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};
const getDomainsList = async () => {
	try {
		const response = await fetch(urlApi + `/Domaines`, {
			method: "GET",
			headers: {
				"Content-Type": "application/json"
			}
		});
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};
const setDomain = async ({ accessToken, domain }) => {
	try {
		const response = await fetch(urlApi + "/Domaines", {
			headers: {
				Authorization: accessToken,
				"Content-Type": "application/json"
			},
			method: "Post",
			body: JSON.stringify(domain)
		});
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};
const getSingleDomain = async ({ currentUser, Id }) => {
	try {
		const response = await fetch(urlApi + `/Domaines/` + Id, {
			method: "GET",
			headers: {
				Authorization: currentUser,
				"Content-Type": "application/json"
			}
		});
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};
const deleteDomain = async ({ accessToken, id }) => {
	try {
		const response = await fetch(urlApi + `/Domaines/${id}`, {
			method: "DELETE",
			headers: {
				Authorization: accessToken,
				"Content-Type": "application/json"
			}
		});
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};
const updateDomain = async ({ currentUser, Id, domain }) => {
	try {
		console.log(domain);
		const response = await fetch(urlApi + `/Domaines/` + Id, {
			method: "PATCH",
			headers: {
				Authorization: currentUser,
				"Content-Type": "application/json"
			},
			body: JSON.stringify(domain)
		});
		if (!response.ok) {
			throw await response.json();
		}

		const data = await response.json();
		console.log(data);
		return data;
	} catch (err) {
		throw err;
	}
};
export {
	getDomainsList,
	deleteDomain,
	updateDomain,
	getSingleDomain,
	setDomain,
	getDomainsListByToken
};
