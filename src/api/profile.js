import { urlApi } from "constants/defaultValues";

const getAllProfilesWithoutlimit = async ({
	accessToken,
	profileType = "profile"
}) => {
	try {
		const orderOptions = `"order":"firstName%20Asc"`;
		const response = await fetch(
			urlApi +
				`/${profileType}/?filter={ 
					"include":[ 
					 
					   { 
						"relation":"User"
					 },
					 {
					  "relation":"Activities"
					 }
				
					],
				${orderOptions}
				
				 }`,
			{
				method: "GET",
				headers: {
					Authorization: accessToken,
					"Content-Type": "application/json"
				}
			}
		);
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};

/**
 * get all profiles
 * @param {string} accessToken user token
 * @param {string} profileType table name
 * @param {number} currentPage number of page
 * @param {number} records_per_page limit of recorders per page
 * @param {string} search get users with first name
 * @param {string} orderOption sorted data
 *
 */
const getAllProfiles = async ({
	accessToken,
	profileType = "profile",
	currentPage,
	records_per_page,
	search,
	orderOption
}) => {
	try {
		const searchFilter =
			search !== "" ? `"where":{"firstName":{"like":"${search}"}},` : "";
		const orderBy = "";
		// orderOption !== null && (orderOption === "google" || orderOption === "fb")
		// 	? "&filter[where][User][email][like]=%" +
		// 	  orderOption +
		// 	  "%&filter[order]=createdAt%20DESC"
		//	: "";
		const orderOptions = `"order":"firstName%20Asc"`;
		const response = await fetch(
			urlApi +
				// `/${profileType}/?filter[include]=Avatar&filter[include]=Cover&filter[include]=User&filter[include]=Activities&filter[include]=Categories&filter[include]=FollowRequest&filter[include]=ReceivedFollowRequest&filter[include]=Followers&filter[include]=ParticipatedActivities&filter[limit]=${records_per_page}&filter[skip]=${(currentPage -
				// 	1) *
				// 	records_per_page}${searchFilter}${orderOptions}${orderBy}`
				`/${profileType}/?filter={ 
					"include":[ 
					   { 
						  "relation":"BlockedProfile",
						  "scope":{ 
							 "include":"fkBlockeduserUsertoblockrel"
						  }
					   },
					   { 
						  "relation":"Avatar"
					   },
					   {
						"relation":"Cover"
					   },
					   { 
						"relation":"User"
					 },
					 {
					  "relation":"Activities"
					 },
					 { 
						"relation":"Categories"
					 },
					 {
					  "relation":"FollowRequest"
					 },
					 { 
					  "relation":"ReceivedFollowRequest"
				   },
				   {
					"relation":"Followers"
				   },
				   { 
					  "relation":"ParticipatedActivities"
				   }
					],
				"limit":${records_per_page},
				"skip":${(currentPage - 1) * records_per_page},
				${searchFilter}${orderOptions}${orderBy}
				
				 }`,
			{
				method: "GET",
				headers: {
					Authorization: accessToken,
					"Content-Type": "application/json"
				}
			}
		);
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};
/**
 * Get user included activities
 * @param {string} accessToken
 */

const getUserActivities = async ({ accessToken }) => {
	try {
		const response = await fetch(
			urlApi +
				`/profile/?filter={ 
					"include":[ 
					   { 
						  "relation":"Activities",
						  "scope":{ 
							 "include":"Address"
						  }
					   },
					   { 
						  "relation":"Avatar"
					   },
					   {
						"relation":"User"
					   }
					]
				 }`,
			{
				method: "GET",
				headers: {
					Authorization: accessToken,
					"Content-Type": "application/json"
				}
			}
		);
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};

/**
 * Update user token
 * @param {string} accessToken
 * @param {object} data
 */
const UpdateUserToken = async (accessToken = null, userdata) => {
	try {
		const response = await fetch(urlApi + `/profile/updateToken`, {
			method: "POST",
			headers: {
				Authorization: accessToken,
				"Content-Type": "application/json"
			},
			body: JSON.stringify(userdata)
		});
		console.log("response === >", response);
		if (!response.ok) {
			throw await response.json();
		}
		const data = await response.json();
		return data;
	} catch (err) {
		throw err;
	}
};

export {
	getAllProfiles,
	getUserActivities,
	getAllProfilesWithoutlimit,
	UpdateUserToken
};
