export const firebaseConfigExpert = {
	apiKey: process.env.REACT_APP_EXPERT_APIKEY,
	authDomain: process.env.REACT_APP_EXPERT_AUTHDOMAIN,
	databaseURL: process.env.REACT_APP_EXPERT_DATABASEURL,
	projectId: process.env.REACT_APP_EXPERT_PROJECTID,
	storageBucket: "",
	messagingSenderId: process.env.REACT_APP_EXPERT_MESSAGINGSENDERID,
	appId: process.env.REACT_APP_EXPERT_APPID
};

export const firebaseConfigCustomer = {
	apiKey: process.env.REACT_APP_CUSTOMER_APIKEY,
	authDomain: process.env.REACT_APP_CUSTOMER_AUTHDOMAIN,
	databaseURL: process.env.REACT_APP_CUSTOMER_DATABASEURL,
	projectId: process.env.REACT_APP_CUSTOMER_PROJECTID,
	storageBucket: "",
	messagingSenderId: process.env.REACT_APP_CUSTOMER_MESSAGINGSENDERID,
	appId: process.env.REACT_APP_CUSTOMER_APPID
};
