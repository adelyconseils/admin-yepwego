import firebase from "firebase";
import "firebase/auth";
import { firebaseConfigExpert, firebaseConfigCustomer } from "./config";
//Init Firebase
const appExpert = firebase.initializeApp(firebaseConfigExpert);
const appCustomer = firebase.initializeApp(
	firebaseConfigCustomer,
	"appCustomer"
);
//Configuration multiple firebase projets
//For more info see https://firebase.google.com/docs/database/usage/sharding
const authExpert = firebase.auth();
const authCustomer = firebase.auth(appCustomer);

export { authExpert, authCustomer };
