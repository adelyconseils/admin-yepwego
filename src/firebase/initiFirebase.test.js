describe("Firebase init", () => {
	afterEach(() => {
		jest.resetModules();
	});
});
/**
 * Tester si Firebase est initialiser avec les variables d'environnements
 * définie dans les fichiers .env
 */
test("Firebase initializeApp is called with firebase environments variables", () => {
	const firebase = require("firebase");
	//Mock firebase module
	jest.mock("firebase");
	//Assign env variables at the run time
	process.env = {
		REACT_APP_EXPERT_APIKEY: "EXPERT_APIKEY",
		REACT_APP_EXPERT_AUTHDOMAIN: "EXPERT_AUTHDOMAIN",
		REACT_APP_EXPERT_DATABASEURL: "EXPERT_DATABASEURL",
		REACT_APP_EXPERT_PROJECTID: "EXPERT_PROJECTID",
		REACT_APP_EXPERT_MESSAGINGSENDERID: "EXPERT_MESSAGINGSENDERID",
		REACT_APP_EXPERT_APPID: "EXPERT_APPID",
		REACT_APP_CUSTOMER_APIKEY: "CUSTOMER_APIKEY",
		REACT_APP_CUSTOMER_AUTHDOMAIN: "CUSTOMER_AUTHDOMAIN",
		REACT_APP_CUSTOMER_DATABASEURL: "CUSTOMER_DATABASEURL",
		REACT_APP_CUSTOMER_PROJECTID: "CUSTOMER_PROJECTID",
		REACT_APP_CUSTOMER_MESSAGINGSENDERID: "CUSTOMER_MESSAGINGSENDERID",
		REACT_APP_CUSTOMER_APPID: "CUSTOMER_APPID"
	};

	const expectedConfigExpert = {
		apiKey: "EXPERT_APIKEY",
		authDomain: "EXPERT_AUTHDOMAIN",
		databaseURL: "EXPERT_DATABASEURL",
		projectId: "EXPERT_PROJECTID",
		storageBucket: "",
		messagingSenderId: "EXPERT_MESSAGINGSENDERID",
		appId: "EXPERT_APPID"
	};
	const expectedConfigCustomer = {
		apiKey: "CUSTOMER_APIKEY",
		authDomain: "CUSTOMER_AUTHDOMAIN",
		databaseURL: "CUSTOMER_DATABASEURL",
		projectId: "CUSTOMER_PROJECTID",
		storageBucket: "",
		messagingSenderId: "CUSTOMER_MESSAGINGSENDERID",
		appId: "CUSTOMER_APPID"
	};
	//firebase.initializeApp = jest.fn();
	require("./index");
	expect(firebase.initializeApp.mock.calls[0][0]).toEqual(expectedConfigExpert);
	expect(firebase.initializeApp.mock.calls[1][0]).toEqual(
		expectedConfigCustomer
	);
	expect(firebase.initializeApp).toBeCalledTimes(2);
});
