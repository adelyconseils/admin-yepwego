import React from "react";

const Icon = props => {
	const style = {
		fontSize: "20px",
		lineHeight: "33px",
		color: props.color
	};
	return <i className={props.icon} style={style} />;
};

export default Icon;
