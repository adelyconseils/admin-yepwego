import React from "react";

export const Chips = props => {
	const { title, color } = props;
	const style = {
		backgroundColor: color,
		borderRadius: "10px",
		color: "#fff",
		padding: "2px 9px",
		fontSize: "11px",
		lineHeight: "15px"
	};
	return (
		<span style={style}>
			<span>{title}</span>
		</span>
	);
};

export default Chips;
