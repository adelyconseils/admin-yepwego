import React from "react";
import { Button } from "reactstrap";
//Message d'exceptions
import exceptions from "lng/exceptions/";
//Api
import { insertException } from "api/exception";
//Firebase
import firebase from "firebase";
//import { auth } from "../../firebase";
import { requestCodeValidation, confirmPhoneCode } from "api/firebaseAuth";

//Toast
import { toast } from "react-toastify";

//Custom components
import InputPhone from "components/InputPhone";
import ModalValidationCode from "./ModalValidationCode";

class PhoneValidation extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			phoneNumber: this.props.phoneNumber || "",
			profileType: this.props.profileType,
			isValidePhoneNumber: this.props.isValidePhoneNumber || false,
			disableActionVerifyPhoneNumber: true,
			isLoadingRequestCode: false, // Loader pour le request code
			isValidateCodeModalOpen: false, // État de modal confirmation code reçue
			confirmationResult: null,
			erreurCodeValidation: false
		};
	}

	/**
	 * Toggle modal vérification de code reçue
	 */
	toggleValidateCodeModal = () =>
		this.setState(prevState => ({
			isValidateCodeModalOpen: !prevState.isValidateCodeModalOpen
		}));

	/**
	 * 1. Gérer le remplissage de numéro de téléphone dans le state
	 * 2. Activer l'action vérification si le numéro de téléphone est valide
	 */
	handleChange = getPhoneNumber => {
		const { phoneNumber } = this.state;
		if (phoneNumber !== getPhoneNumber) {
			this.setState(
				{ phoneNumber: getPhoneNumber, disableActionVerifyPhoneNumber: true },
				() => {
					let phone = getPhoneNumber;
					if (this.props.regex && this.props.regex !== "") {
						const replace = this.props.regex;
						const regRxp = new RegExp(replace, "g");
						phone = phone.replace(regRxp, "");
					}
					if (phone.length > 8) {
						this.setState({ disableActionVerifyPhoneNumber: false });
					}
				}
			);
		}
	};

	/*******************************************************/
	/***********     REQUEST CODE TO SMS  *****************/
	/*******************************************************/
	/**
	 * Action clique sur la bouton vérification N° de téléphone
	 * l'action est désactiver si le numéro de téléphone n'est pas valide
	 * 1. Envoyer un code
	 * 2. Ouvrir le modal
	 */
	verifyPhoneNumber = event => {
		event.preventDefault();
		//Vérifier si le numéro de téléphone insérer est déja valider
		const { isValidePhoneNumber, phoneNumber } = this.state;
		if (isValidePhoneNumber && phoneNumber) {
			this.props.getPhoneNumber(this.state.phoneNumber);
			return "";
		}
		this.requestCodeValidation({ withOpenModal: true });
	};

	/**
	 * @description Envoyer un code de validation par SMS
	 * @param withOpenModal: Ouvrir le modal si besoin
	 */
	requestCodeValidation = ({ withOpenModal = false }) => {
		const appVerifier = window.recaptchaVerifier;
		const { phoneNumber, profileType } = this.state;
		this.setState({ isLoadingRequestCode: true }, () =>
			requestCodeValidation(phoneNumber, appVerifier, profileType)
				.then(confirmationResult => {
					window.confirmationResult = confirmationResult;
					this.setState({ isLoadingRequestCode: false }, () => {
						if (withOpenModal) this.toggleValidateCodeModal();
					});
					const m = this.props.successMessageSendCode.replace(
						"{phoneNumber}",
						this.state.phoneNumber
					);
					toast.success(m);
				})
				.catch(error => {
					let message = "Une erreur est survenue. veuillez réessayer";
					if (error.code && exceptions.phoneVerification[error.code]) {
						message = exceptions.phoneVerification[error.code];
					}
					error = {
						...error,
						file: "src/components/PhoneValidation/index.js",
						fn: "requestCodeValidation"
					};
					insertException({ description: JSON.stringify(error) });
					toast.error(message);
					window.recaptchaVerifier.render();
					this.setState({ isLoadingRequestCode: false });
				})
		);
	};

	/*******************************************************/
	/***********     VALIDATE REQUESTED CODE *****************/
	/*******************************************************/
	validateCodeAction = code => {
		this.setState(
			{ isLoadingConfirmCode: true, erreurCodeValidation: false },
			() => this.requestConfirmPhoneCode(code)
		);
	};
	requestConfirmPhoneCode = code => {
		const { confirmationResult: { verificationId } = null } = window;
		const { profileType } = this.state;
		console.log(verificationId);
		confirmPhoneCode(verificationId, code, profileType)
			.then(response => {
				this.setState({
					isLoadingConfirmCode: false,
					erreurCodeValidation: false
				});
				this.props.getPhoneNumber(this.state.phoneNumber);
				this.toggleValidateCodeModal();
				toast.success(this.props.successMessageValidateCode);
			})
			.catch(error => {
				insertException({ description: JSON.stringify(error) });
				console.log(error);
				this.setState({
					isLoadingConfirmCode: false,
					erreurCodeValidation: true
				});
				window.recaptchaVerifier.render();
			});
	};

	componentDidMount() {
		//Activer captcha Google
		//See => https://firebase.google.com/docs/auth/web/phone-auth
		//auth.languageCode = this.props.captchaLanguage;
		window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
			"verification-phone-number",
			{
				size: "invisible",
				"expired-callback": function() {},
				"error-callback": function() {}
			}
		);
		//Vérifier si le numéro de téléphone insérer est déja valider
		const { isValidePhoneNumber, phoneNumber } = this.state;
		if (isValidePhoneNumber && phoneNumber) {
			this.setState({ disableActionVerifyPhoneNumber: false });
		}
	}

	render() {
		const {
			phoneNumber,
			isValidateCodeModalOpen,
			disableActionVerifyPhoneNumber,
			isLoadingRequestCode,
			isLoadingConfirmCode,
			erreurCodeValidation
		} = this.state;
		const {
			defaultCountry = "fr",
			onlyCountries = ["fr"],
			textButtonAction = "Suivant",
			CSSClass
		} = this.props;
		return (
			<div className={CSSClass}>
				<InputPhone
					onChange={this.handleChange}
					value={phoneNumber}
					defaultCountry={defaultCountry}
					onlyCountries={onlyCountries}
				/>
				<Button
					color="dark"
					outline
					className="mt-4 d-block"
					outline
					disabled={disableActionVerifyPhoneNumber || isLoadingRequestCode}
					onClick={this.verifyPhoneNumber}
					id="verification-phone-number"
				>
					{isLoadingRequestCode ? "Chargement ..." : textButtonAction}
				</Button>
				<ModalValidationCode
					isValidateCodeModalOpen={isValidateCodeModalOpen}
					toggleValidateCodeModal={this.toggleValidateCodeModal}
					phoneNumber={phoneNumber}
					requestCodeValidation={this.requestCodeValidation}
					isLoadingRequestCode={isLoadingRequestCode}
					requestConfirmPhoneCode={this.validateCodeAction}
					isLoadingConfirmCode={isLoadingConfirmCode}
					erreurCodeValidation={erreurCodeValidation}
				/>
			</div>
		);
	}
}

export default PhoneValidation;

/**
 * Exemple comment utilisé le compostant
 * 				<PhoneValidation
					captchaLanguage="fr"
					textButtonAction="Suivant"
					defaultCountry="fr"
					onlyCountries={["fr", "tn"]}
					regex={"[\\s+]"}
					getPhoneNumber={this.getPhoneNumber}
					successMessageSendCode="Nous venons d'envoyer un message texte avec un nouveau code de vérification au numéro de téléphone: {phoneNumber} "
					successMessageValidateCode="Merci! Votre numéro de téléphone a bien été validé"
				/>
 */
