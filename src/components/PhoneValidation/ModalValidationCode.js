import React from "react";
//UX
import { Modal, ModalBody, Button } from "reactstrap";
import ReactCodeInput from "react-code-input";
import { ClimbingBoxLoader } from "react-spinners";
const ModalValidationCode = props => {
	let code = null;
	const handelCodeInput = val => (code = val);
	const confirmPhoneCode = () => {
		props.requestConfirmPhoneCode(code);
	};
	const {
		isValidateCodeModalOpen,
		toggleValidateCodeModal,
		phoneNumber,
		requestCodeValidation,
		isLoadingRequestCode,
		isLoadingConfirmCode,
		erreurCodeValidation
	} = props;
	return (
		<Modal
			isOpen={isValidateCodeModalOpen}
			toggle={toggleValidateCodeModal}
			backdrop={false}
		>
			<div style={{ minHeight: "450px" }}>
				{isLoadingRequestCode || isLoadingConfirmCode ? (
					<ClimbingBoxLoader
						sizeUnit={"px"}
						size={20}
						color={"#787878"}
						css="position: absolute !important;
							left: 38%;
							bottom: 45% !important;"
						loading
					/>
				) : (
					<>
						<ModalBody>
							<h2 className="text-center mb-4">
								Demander votre code à usage unique
							</h2>
							<p className="list-item-heading">
								La confirmation de votre numéro de téléphone est une couche
								supplémentaire de vérification qui nous aide à confirmer que
								vous n’êtes pas un bot.
							</p>
							<p className="list-item-heading mb-4">
								{" "}
								Nous venons d'envoyer un message texte avec un nouveau code de
								vérification au numéro de téléphone{" "}
								<strong>
									<i>{phoneNumber}</i>
								</strong>
							</p>
							<div className="d-flex flex-column align-items-center">
								{erreurCodeValidation && (
									<div
										class="alert alert-danger text-center fade show"
										role="alert"
									>
										<span>
											Le code que vous avez entré est invalide !<br /> Le
											problème persiste encore?{" "}
											<span
												className="text-decoration cursor"
												onClick={requestCodeValidation}
											>
												Cliquer ici pour envoyer un nouveau code
											</span>
											<br /> Le problème persiste encore? cliquer ici pour nous
											contacter
										</span>
									</div>
								)}
								<ReactCodeInput
									onChange={handelCodeInput}
									name="code"
									type="number"
									fields={6}
									{...inputCodeStyle}
								/>
							</div>
							<Button
								color="dark"
								className=" mt-4 d-block mx-auto"
								disabled={isLoadingConfirmCode}
								onClick={confirmPhoneCode}
							>
								{isLoadingConfirmCode ? "chargement ..." : "Confirmer"}
							</Button>
							<p className="mt-4">
								Vous n'avez pas reçu de code?{" "}
								<span
									className="text-decoration cursor"
									onClick={requestCodeValidation}
								>
									Demander un nouveau code.
								</span>
							</p>
						</ModalBody>
					</>
				)}
			</div>
		</Modal>
	);
};
const inputCodeStyle = {
	inputStyle: {
		fontFamily: "monospace",
		margin: "4px",
		MozAppearance: "textfield",
		width: "42px",
		borderRadius: "3px",
		fontSize: "28px",
		height: "46px",
		paddingLeft: "10px",
		backgroundColor: "#f3f3f3",
		color: "#787878",
		border: "1px solid #787878"
	},
	inputStyleInvalid: {}
};
export default ModalValidationCode;
