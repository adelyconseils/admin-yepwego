import React from "react";
//UX
import { Button } from "reactstrap";
export const SocialButton = props => {
	const { link = null, title, icon, cssClass, action = () => false } = props;
	const actionHandle = () => {
		console.log(typeof action);
		return typeof action === "function"
			? action
			: () => (window.location.href = action);
	};
	return (
		<Button
			color="light"
			outline
			className={"btn-lg mr-4 px-4 d-flex " + cssClass}
			type="button"
			onClick={actionHandle()}
		>
			<span
				className={"glyph-icon pr-2 " + icon}
				style={{ fontSize: "1.2rem", lineHeight: "1.4rem" }}
			/>
			{title}
		</Button>
	);
};
