import React, { Component } from "react";

class RandomImages extends Component {
	render() {
		const { image = "bg-register-1.jpg" } = this.props;
		return (
			<div
				style={{
					height: "100%",
					backgroundSize: "cover",
					backgroundPosition: "top right",
					backgroundImage: `url("${process.env.PUBLIC_URL}/img/${image}")`
				}}
			/>
		);
	}
}

export default RandomImages;
