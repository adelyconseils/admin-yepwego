import React, { Component } from "react";
import logo from "assets/img/user-13-icon-256.png";
import ThumbnailLetters from "components/ThumbnailLetters";
class Imagecard extends Component {
	constructor(props) {
		super(props);

		this.state = {
			src: props.src,
			errored: false
		};
	}

	onError = () => {
		if (!this.state.errored) {
			this.setState({
				src: logo,
				errored: true
			});
		}
	};

	render() {
		const { src, errored } = this.state;
		const { src: _1, fallbackSrc: _2, ...props } = this.props;

		if (props.userCard) {
			if (!errored) {
				return <img src={src} onError={this.onError} {...props} />;
			} else {
				return (
					<ThumbnailLetters
						small
						rounded={props.rounded}
						text={props.text}
						className="m-4"
						color="info"
					/>
				);
			}
		} else {
			return <img src={src} onError={this.onError} {...props} />;
		}
	}
}
export default Imagecard;
