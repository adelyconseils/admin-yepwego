import React from "react";
import { StripeProvider, Elements } from "react-stripe-elements";
//Public Key Stripe
import { stripe_apiKey } from "constants/defaultValues";
//Style
import "./style.scss";
export default props => {
	return (
		<StripeProvider apiKey={stripe_apiKey}>
			<Elements>{props.children}</Elements>
		</StripeProvider>
	);
};
