import React from "react";
import { injectStripe, CardElement } from "react-stripe-elements";
import StripeProvider from "components/StripePayement";

//UX
import { Input, Form, FormGroup, Label, Button } from "reactstrap";

const createOptions = (fontSize, padding) => {
	return {
		style: {
			base: {
				fontSize,
				color: "#424770",
				letterSpacing: "0.025em",
				fontFamily: "Source Code Pro, monospace",
				"::placeholder": {
					color: "#aab7c4"
				},
				padding
			},
			invalid: {
				color: "#9e2146"
			}
		}
	};
};
class _IbanForm extends React.Component {
	state = {
		errorMessage: ""
	};

	handleChange = ({ error }) => {
		if (error) {
			this.setState({ errorMessage: error.message });
		}
	};
	handleSubmit = ev => {
		ev.preventDefault();
		if (this.props.stripe) {
			this.props.stripe
				.createSource({
					type: "sepa_debit",
					currency: "eur",
					owner: {
						name: ev.target.name.value,
						email: ev.target.email.value
					},
					mandate: {
						notification_method: "email"
					}
				})
				.then(payload => console.log("[source]", payload));
		} else {
			console.log("Stripe.js hasn't loaded yet.");
		}
	};
	render() {
		return (
			<Form onSubmit={this.handleSubmit} className="formElement">
				<FormGroup>
					<Label for="name">Nom & Prénom</Label>
					<Input name="name" type="text" placeholder="Jane Doe" required />
				</FormGroup>
				<FormGroup>
					<Label for="email">Email</Label>
				</FormGroup>
				<FormGroup>
					<Label for="iban">IBAN</Label>
					<CardElement onChange={this.handleChange} {...createOptions()} />
					<div className="error" role="alert">
						{this.state.errorMessage}
					</div>
				</FormGroup>
				<Button color="primary" outline className="float-right" type="submit">
					Paiement
				</Button>
			</Form>
		);
	}
}
const IbanForm = injectStripe(_IbanForm);

export default () => {
	return (
		<StripeProvider>
			<IbanForm />
		</StripeProvider>
	);
};
