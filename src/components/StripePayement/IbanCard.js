import React from "react";
import IbanForm from "components/StripePayement/IbanForm";
//UX
import { Card, CardBody, CardTitle } from "reactstrap";

class IbanCard extends React.Component {
	render() {
		return (
			<Card>
				<CardBody>
					<CardTitle>
						<h5>Paiement par RIB</h5>
					</CardTitle>
					<IbanForm />
				</CardBody>
			</Card>
		);
	}
}

export default IbanCard;
