import React from "react";
import PayementCardForm from "components/StripePayement/PayementCardForm";
//UX
import { Card, CardBody, CardTitle } from "reactstrap";

class PayementCard extends React.Component {
	render() {
		return (
			<Card>
				<CardBody>
					<CardTitle>
						<h5>Paiement par carte bancaire </h5>
					</CardTitle>
					<PayementCardForm />
				</CardBody>
			</Card>
		);
	}
}

export default PayementCard;
