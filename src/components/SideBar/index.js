import React, { useState, useEffect } from "react";
import { Card, CardBody, ListGroup, ListGroupItem } from "reactstrap";
import { Link, withRouter } from "react-router-dom";
import classnames from "classnames";

function SideBar(props) {
	const [selected, setSelctedItem] = useState("/");
	const path = props.location.pathname;
	console.log(path.substring(path.lastIndexOf("/")));

	const cleanPath = () => {
		return path.substring(path.lastIndexOf("/"));
	};
	useEffect(() => {
		if (path != selected) {
			setSelctedItem(cleanPath);
		}
	}, [path]);
	console.log(selected);
	return (
		<>
			<ListGroup>
				<Link to="identity">
					<ListGroupItem
						onClick={() => setSelctedItem(cleanPath())}
						className={classnames({ active: selected == "/identity" })}
						tag="button"
						action
					>
						<h4>
							<i class="fas fa-user" /> Identité
						</h4>
					</ListGroupItem>
				</Link>
				<ListGroupItem
					onClick={() => setSelctedItem(2)}
					className={classnames({ active: selected === 2 })}
					tag="button"
					action
				>
					<h4>
						<i class="fas fa-envelope-open" /> Email
					</h4>
				</ListGroupItem>
				<ListGroupItem
					onClick={() => setSelctedItem(3)}
					className={classnames({ active: selected === 3 })}
					tag="button"
					action
				>
					<h4>
						<i class="fas fa-phone" /> Téléphone
					</h4>
				</ListGroupItem>
				<Link to="password">
					<ListGroupItem
						onClick={() => setSelctedItem(cleanPath())}
						tag="button"
						action
						className={classnames({ active: selected === "/password" })}
					>
						<h4>
							<i class="fas fa-shield-alt" /> Securité
						</h4>
					</ListGroupItem>
				</Link>
			</ListGroup>
		</>
	);
}

export default withRouter(SideBar);
