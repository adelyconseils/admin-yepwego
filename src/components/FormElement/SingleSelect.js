import React from "react";
import Select from "react-select";

const SingleSelect = props => {
	const options = Object.values(props.options).map(option => ({
		label: option[props.keyToShow],
		value: option.id,
		key: option.id
	}));

	let selected = "";
	if (props.selectedOption && props.selectedOption.key) {
		selected = options[
			options.findIndex(k => k.key === props.selectedOption.key)
		]
			? options[options.findIndex(k => k.key === props.selectedOption.key)]
			: "";
	}
	return (
		<Select
			className={"react-select " + props.className}
			classNamePrefix="react-select"
			name={props.name}
			placeholder={props.placeholder}
			isLoading={props.loading}
			value={selected}
			options={options}
			isDisabled={props.isDisabled}
			onChange={props.controlFunc}
		/>
	);
};

export default SingleSelect;
