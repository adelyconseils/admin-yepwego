import React from "react";
import PropTypes from "prop-types";
import { Input } from "reactstrap";

const SingleInput = props => (
	<Input
		name={props.name}
		type={props.inputType}
		value={props.content}
		className={props.className}
		onChange={props.controlFunc}
		placeholder={props.placeholder}
	/>
);

/* 
TODO : prop type validation
 SingleInput.propTypes = {
	inputType: PropTypes.oneOf(['text', 'number']).isRequired,
	name: PropTypes.string.isRequired,
	controlFunc: PropTypes.func.isRequired,
	content: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number,
	]).isRequired,
	placeholder: PropTypes.string,
} */
export default SingleInput;
