import React from "react";
import { Label, CustomInput } from "reactstrap";

const CheckBox = props => {
	return (
		<>
			<Label>{props.label}</Label>
			<CustomInput
				type="checkbox"
				id={props.name}
				name={props.name}
				label={props.placeholder}
				checked={props.value}
				onChange={props.controlFunc}
			/>
		</>
	);
};

export default CheckBox;
