import React from "react";
import { Input } from "reactstrap";

const TextArea = props => {
	const styleTextArea = {
		minHeight: "150px"
	};
	return (
		<Input
			name={props.name}
			id={props.name}
			style={styleTextArea}
			className={props.className}
			type={props.type}
			value={props.content}
			onChange={props.controlFunc}
			placeholder={props.placeholder}
		/>
	);
};

export default TextArea;
