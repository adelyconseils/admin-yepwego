import React, { Fragment } from "react";
/*UX*/
import Switch from "react-switch";

const SingleSwitch = props => {
	return (
		<Fragment>
			<label>{props.title}</label>
			<Switch
				name={props.name}
				checked={props.value === false}
				onChange={props.controlFunc}
				onClick={props.controlFunc}
				disabled={props.disabled === false}
			/>
		</Fragment>
	);
};

export default SingleSwitch;
