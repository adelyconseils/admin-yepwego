import React, { Component } from "react";
import classnames from "classnames";
import { TabContent, Nav, NavItem, NavLink } from "reactstrap";
import { Switch, Route, withRouter, Link } from "react-router-dom";
import SettingType from "../settingTypes/index";

export class Tab extends Component {
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);
		this.state = {
			activeTab: "1"
		};
	}

	toggle(tab) {
		if (this.state.activeTab !== tab) {
			this.setState({
				activeTab: tab
			});
		}
	}
	render() {
		const { tabHeader, match } = this.props;
		return (
			<div>
				<Nav tabs className="separator-tabs ml-0 mb-5">
					{tabHeader.map(item => {
						return (
							<NavItem key={item.id}>
								<div className="nav-link">
									<NavLink
										className={classnames({
											active: this.state.activeTab === item.id
										})}
										onClick={() => {
											this.toggle(item.id);
										}}
									>
										<Link to={`${match.url}/${item.slug}`}>{item.name} </Link>
									</NavLink>
								</div>
							</NavItem>
						);
					})}
				</Nav>
				<div>
					<Switch>
						{tabHeader.map(item => {
							return (
								<Route
									key={item.id}
									exact
									path={`${match.url}/${item.slug}`}
									render={() => (
										<SettingType
											IdSettingGroup={item.id}
											nameSettingGroup={item.name}
										/>
									)}
								/>
							);
						})}
					</Switch>
				</div>
			</div>
		);
	}
}

export default withRouter(Tab);
