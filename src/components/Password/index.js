import React from "react";

import { Row, FormGroup, Label, Input, FormFeedback } from "reactstrap";

import { Colxx } from "components/CustomBootstrap";

//HELPERS
import { handleInputError } from "util/Validation";

//Custom components

class Password extends React.Component {
	constructor(props) {
		super(props);
		this.INITIAL_STATE = {
			isLoading: false,
			password: "",
			confirmationPassword: "",
			// object erreurs formulaire
			errors: {}
		};
		this.state = this.INITIAL_STATE;
	}

	//Set the state of form elements
	handelFormElement = event =>
		this.setState({ [event.target.name]: event.target.value });

	/**
	 * Action cliquer sur le bouton action
	 */
	handleActionSubmitForm = () => {
		return new Promise((resolve, reject) => {
			if (!this.isValidForm()) {
				reject(false);
			}
			const { password } = this.state;
			resolve({ password });
		});
	};
	/**
	 * Vérifier la validité de la formulaire
	 */
	isValidForm = () => {
		//Récupérer les states
		const { password, confirmationPassword } = this.state;

		//Initialiser l'object erreur
		let errors = {};

		//Vérifier les champs
		if (password.length < 6) {
			errors.password = { message: "Le mot de passe est invalide" };
		}
		if (password !== confirmationPassword) {
			errors.confirmationPassword = {
				message: "Le mot de passe ne correspond pas"
			};
		}
		if (Object.entries(errors).length !== 0) {
			return this.setState({ errors }, () => false);
		}
		return true;
	};

	render() {
		const { password, confirmationPassword, errors } = this.state;
		return (
			<Row>
				<Colxx xxs="12" md="6">
					<FormGroup>
						<Label for="password"> Mot de passe</Label>
						<Input
							type="password"
							name="password"
							placeholder="Veuillez saisir un mot de passe"
							value={password}
							invalid={handleInputError(errors, "password")}
							onChange={this.handelFormElement}
						/>
						<FormFeedback>
							{errors.password && errors.password.message}
						</FormFeedback>
					</FormGroup>
				</Colxx>
				<Colxx xxs="12" md="6">
					<FormGroup>
						<Label for="confirmationPassword">Confirmer le mot de passe</Label>
						<Input
							type="password"
							name="confirmationPassword"
							placeholder="Confirmer votre mot de passe"
							value={confirmationPassword}
							onChange={this.handelFormElement}
							invalid={handleInputError(errors, "confirmationPassword")}
						/>
						<FormFeedback>
							{errors.confirmationPassword &&
								errors.confirmationPassword.message}
						</FormFeedback>
					</FormGroup>
				</Colxx>
			</Row>
		);
	}
}

export default Password;

/**
* Exemple comment utiliser le compostant:
* <Password
	/>
 */
