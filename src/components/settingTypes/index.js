import React, { Component } from "react";
import {
	Row,
	Col,
	Button,
	Modal,
	ModalBody,
	ModalFooter,
	ModalHeader,
	Form,
	FormGroup,
	Input,
	Label,
	FormFeedback,
	Card,
	CustomInput,
	Collapse,
	Spinner
} from "reactstrap";
import { connect } from "react-redux";
import {
	getSettingTypesByGroup,
	setSettingType,
	deleteSettingType,
	setSetting
} from "api/setting.js";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import { handleInputError } from "util/Validation";
import { toast } from "react-toastify";
import Select from "react-select";
const SELECT_DATA = [
	{ label: "checkbox", value: "checkbox", key: 0 },
	{ label: "text", value: "text", key: 1 },
	{ label: "textarea", value: "textarea", key: 2 },
	{ label: "date", value: "date", key: 3 },
	{ label: "number", value: "number", key: 4 }
];

class SettingType extends Component {
	constructor(props) {
		super(props);
		this.state = {
			IdSettingGroup: props.IdSettingGroup,
			SettingGroup: props.nameSettingGroup,
			modalRight: false,
			name: "",
			description: "",
			isActivated: false,
			errors: [],
			settingTypes: [],
			collapse: false,
			selectedId: "",
			collapseSetting: false,
			slug: "",
			nameSetting: "",
			descriptionSetting: "",
			isActivatedSetting: false,
			isLoading: false,
			selectedOption: "",
			content: ""
		};
	}

	clearFrom = () => {
		this.setState({
			name: "",
			description: "",
			isActivated: false,
			nameSetting: "",
			descriptionSetting: "",
			isActivatedSetting: false
		});
	};

	componentDidMount() {
		const { currentUser, IdSettingGroup } = this.props;

		getSettingTypesByGroup({
			accessToken: currentUser.id,
			id: IdSettingGroup
		})
			.then(response => {
				this.setState({
					settingTypes: response
				});
			})
			.catch(() => {
				toast.error("erreur servenu !!!");
			});
	}
	toggle = IdSelected => {
		this.setState(state => ({
			collapse: !state.collapse,
			selectedId: IdSelected
		}));
	};

	toggleSetting = () => {
		this.setState({
			collapseSetting: !this.state.collapseSetting
		});
	};
	toggleDropdown = () => {
		this.setState(prevState => ({
			dropdownOpen: !prevState.dropdownOpen
		}));
	};
	handelChangeIsActivated = event => {
		this.setState({
			[event.target.name]: !this.state[event.target.name]
		});
	};

	isValidForm = () => {
		let errors = {};
		const { name, description } = this.state;
		if (name.length < 1) {
			errors.name = {
				message: "Le nom ne peut pas etre vide ! ! !"
			};
		}

		if (description.length < 1) {
			errors.description = {
				message: "La description ne peut pas etre vide ! ! !"
			};
		}

		if (Object.entries(errors).length !== 0) {
			return this.setState({ errors }, () => false);
		}

		return true;
	};

	handelChangeElement = event => {
		this.setState({ [event.target.name]: event.target.value });
	};

	toggleRight() {
		this.setState({
			modalRight: !this.state.modalRight
		});
		this.clearFrom();
	}

	handelSubmit = event => {
		event.preventDefault();
		const { currentUser, IdSettingGroup } = this.props;

		const settingType = {
			name: this.state.name,
			description: this.state.description,
			isActivated: this.state.isActivated,
			settingGroupId: IdSettingGroup
		};

		if (this.isValidForm()) {
			setSettingType({ accessToken: currentUser.id, settingType })
				.then(response => {
					const newSetting = this.state.settingTypes.push(response);
					this.setState({
						modalRight: false,
						setSettingTypes: newSetting
					});
					toast.success("setting types ajouté avec succesé ");
				})
				.catch(() => {
					toast.error(" un erreur a été servenu ");
				});
		}
	};

	deleteHandler = id => {
		confirmAlert({
			title: "Confirmer la supprition",
			message: "Esque vous êtes sûrs de supprimer ce type",
			buttons: [
				{
					label: "Yes",
					onClick: () => {
						const { settingTypes } = this.state;
						const { currentUser } = this.props;
						deleteSettingType({ accessToken: currentUser.id, id })
							.then(() => {
								const index = settingTypes
									.map(item => {
										return item.id;
									})
									.indexOf(id);
								settingTypes.splice(index, 1);
								this.setState({ setSettingTypes: settingTypes });
							})
							.catch(() => {
								toast.error("il ya un problem Veuillez réessayer");
							});
					}
				},
				{
					label: "No",
					onClick: () => false
				}
			]
		});

		/*	const { settingTypes } = this.state;
		const { currentUser } = this.props;
		deleteSettingType({ accessToken: currentUser.id, id })
			.then(() => {
				const index = settingTypes
					.map(item => {
						return item.id;
					})
					.indexOf(id);
				settingTypes.splice(index, 1);
				this.setState({ setSettingTypes: settingTypes });
			})
			.catch(() => {
				toast.error("il ya un problem Veuillez réessayer");
			}); */
	};

	isValidFormSetting = () => {
		const {
			errors,
			nameSetting,
			descriptionSetting,
			selectedOption,
			content
		} = this.state;
		if (nameSetting.length < 1) {
			errors.nameSetting = {
				message: "Le nom ne peut pas etre vide ! ! !"
			};
		}

		if (descriptionSetting.length < 1) {
			errors.descriptionSetting = {
				message: "La descriptionSetting ne peut pas etre vide ! ! !"
			};
		}
		if (selectedOption.length < 1) {
			errors.selectedOption = {
				message: "Le type est obligatoire ! ! !"
			};
		}
		if (content.length < 1) {
			errors.content = {
				message: "Le contenu  est obligatoire ! ! !"
			};
		}
		if (Object.entries(errors).length !== 0) {
			return this.setState({ errors }, () => false);
		}

		return true;
	};

	handelSubmitSetting = (e, id) => {
		e.preventDefault();
		if (this.isValidFormSetting()) {
			const setting = {
				name: this.state.nameSetting,
				description: this.state.descriptionSetting,
				slug: this.state.slug,
				settingTypeId: id,
				isActivated: this.state.isActivatedSetting,
				type: this.state.selectedOption
			};

			const { currentUser } = this.props;

			setSetting({ accessToken: currentUser.id, setting: setting })
				.then(() => {
					toast.success("votre paramètre a été ajouter avec succès");
				})
				.catch(() => {
					toast.success("erreur");
				});
		}
	};

	handleChange = selectedOption => {
		this.setState({ selectedOption: selectedOption.value });
	};
	render() {
		const {
			SettingGroup,
			name,
			description,
			isActivated,
			errors,
			settingTypes,
			selectedId,
			collapse,
			collapseSetting,
			isActivatedSetting,
			nameSetting,
			descriptionSetting,
			isLoading,
			selectedOption,
			content
		} = this.state;
		console.log(selectedOption);
		return (
			<>
				<Row
					style={{
						marginBottom: "20px"
					}}
				>
					<Col md="9" sm="9"></Col>
					<Col>
						<Button
							color="primary"
							size="lg"
							onClick={() => this.toggleRight()}
						>
							Ajouter un type de configuration
						</Button>
					</Col>
				</Row>
				{isLoading ? (
					<Spinner></Spinner>
				) : (
					settingTypes.map(item => {
						return (
							<Row key={item.id}>
								<Col md="12" xs="12">
									<Card className="d-flex flex-row mb-3">
										<div className="pl-2 d-flex flex-grow-1 min-width-zero">
											<div className="card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center">
												<h5>{item.name}</h5>
											</div>
											<div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
												<div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
													<Button
														outline
														color={"theme-3"}
														className="icon-button ml-1 view-button no-border"
														onClick={e => this.toggle(item.id)}
													>
														<i className="simple-icon-eye" />
													</Button>

													<Button
														outline
														color={"theme-3"}
														className="icon-button ml-1"
														onClick={() => this.deleteHandler(item.id)}
													>
														<i className="simple-icon-ban" />
													</Button>
												</div>
											</div>
										</div>
									</Card>
									<Collapse
										isOpen={collapse && item.id == selectedId ? true : false}
									>
										<Card className="pt-0">
											{item.description}
											<Button
												outline
												color={"theme-3"}
												className="primary"
												onClick={this.toggleSetting}
											>
												{collapseSetting ? (
													<i className="simple-icon-arrow-up"></i>
												) : (
													"ajouté des setting"
												)}
											</Button>
										</Card>
									</Collapse>
									<Collapse
										isOpen={
											collapseSetting && item.id == selectedId ? true : false
										}
									>
										<Card>
											<Form
												onSubmit={e => this.handelSubmitSetting(e, item.id)}
											>
												<FormGroup>
													<Row>
														<Col md="4">nom</Col>
														<Col>
															<Input
																name="nameSetting"
																invalid={handleInputError(
																	errors,
																	"nameSetting"
																)}
																onChange={this.handelChangeElement}
																value={nameSetting}
															></Input>
															<FormFeedback>
																{errors.nameSetting &&
																	errors.nameSetting.message}
															</FormFeedback>
														</Col>
													</Row>
												</FormGroup>
												<FormGroup>
													<Row>
														<Col md="4">
															<Label>Description</Label>
														</Col>
														<Col>
															<Input
																name="descriptionSetting"
																invalid={handleInputError(
																	errors,
																	"descriptionSetting"
																)}
																onChange={this.handelChangeElement}
																value={descriptionSetting}
																type="textarea"
															></Input>
															<FormFeedback>
																{errors.descriptionSetting &&
																	errors.descriptionSetting.message}
															</FormFeedback>
														</Col>
													</Row>
												</FormGroup>
												<FormGroup>
													<Row>
														<Col md="4">
															<Label>Type</Label>
														</Col>
														<Col>
															<Select
																className="react-select"
																classNamePrefix="react-select"
																name="form-field-name"
																onChange={this.handleChange}
																options={SELECT_DATA}
															>
																<FormFeedback>
																	{errors.selectedOption &&
																		errors.selectedOption.message}
																</FormFeedback>
															</Select>
														</Col>
													</Row>
												</FormGroup>
												<FormGroup>
													<Row>
														<Col md="4">Contenu</Col>
														<Col>
															<Input
																name="content"
																invalid={handleInputError(errors, "content")}
																onChange={this.handelChangeElement}
																value={content}
															></Input>
															<FormFeedback>
																{errors.content && errors.content.message}
															</FormFeedback>
														</Col>
													</Row>
												</FormGroup>
												<FormGroup>
													<Row>
														<Col md="4">
															<Label>Activé</Label>
														</Col>
														<Col>
															<CustomInput
																checked={isActivatedSetting}
																type="checkbox"
																id="isActivated"
																name="isActivatedSetting"
																onClick={this.handelChangeIsActivated}
															/>
														</Col>
													</Row>
												</FormGroup>
												<Button color="primary">Enregistrer</Button>
											</Form>
										</Card>
									</Collapse>
								</Col>
							</Row>
						);
					})
				)}
				{/* ************  display modal section  ************ */}
				<Modal isOpen={this.state.modalRight} wrapClassName="modal-right">
					<ModalHeader>
						Ajouter un type de configuration pour {SettingGroup}
					</ModalHeader>
					<ModalBody>
						<Form onSubmit={this.handelSubmit}>
							<FormGroup>
								<Row>
									<Col md="4">nom de type</Col>
									<Col>
										<Input
											name="name"
											invalid={handleInputError(errors, "name")}
											onChange={this.handelChangeElement}
											value={name}
										></Input>
										<FormFeedback>
											{errors.name && errors.name.message}
										</FormFeedback>
									</Col>
								</Row>
							</FormGroup>
							<FormGroup>
								<Row>
									<Col md="4">
										<Label>Description</Label>
									</Col>
									<Col>
										<Input
											name="description"
											invalid={handleInputError(errors, "description")}
											onChange={this.handelChangeElement}
											value={description}
											type="textarea"
										></Input>
										<FormFeedback>
											{errors.description && errors.description.message}
										</FormFeedback>
									</Col>
								</Row>
							</FormGroup>
							<FormGroup>
								<Row>
									<Col md="4">
										<Label>Activé</Label>
									</Col>
									<Col>
										<CustomInput
											checked={isActivated}
											type="checkbox"
											id="isActivated"
											name="isActivated"
											onClick={this.handelChangeIsActivated}
										/>
									</Col>
								</Row>
							</FormGroup>
						</Form>
					</ModalBody>
					<ModalFooter>
						<Button color="primary" onClick={this.handelSubmit}>
							Enregistrer
						</Button>
						<Button color="secondary" onClick={() => this.toggleRight()}>
							Cancel
						</Button>
					</ModalFooter>
				</Modal>
			</>
		);
	}
}
const mapStateToProps = ({ auth }) => {
	const { user } = auth;
	return { currentUser: user };
};

export default connect(
	mapStateToProps,
	null
)(SettingType);
