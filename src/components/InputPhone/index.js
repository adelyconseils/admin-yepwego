import React from "react";
import ReactPhoneInput from "react-phone-input-2";
import "react-phone-input-2/dist/style.css";

const InputPhone = props => (
	<div>
		<ReactPhoneInput
			defaultCountry={props.defaultCountry}
			onlyCountries={props.onlyCountries}
			value={props.value}
			onChange={props.onChange}
		/>
	</div>
);

export default InputPhone;
