import { chartTooltip } from "../../util/Utils";
export const doughnutChartOptions = {
	legend: {
		position: "bottom",
		labels: {
			padding: 30,
			usePointStyle: true,
			fontSize: 12
		}
	},
	responsive: true,
	maintainAspectRatio: false,
	title: {
		display: false
	},
	cutoutPercentage: 80,
	layout: {
		padding: {
			bottom: 20
		}
	},
	tooltips: chartTooltip
};
