import React from "react";
import { Card, CardBody, CardTitle } from "reactstrap";

import DoughnutChart from "./Doughnut";

const ProductCategoriesDoughnut = ({ deviseUsageChartData }) => {
	return (
		<Card className="h-100">
			<CardBody>
				<CardTitle>
					<div className="d-inline-block">
						<h5 className="font-weight-bold">
							<p>Installations par appareil:</p>
						</h5>
						<span className="text-muted text-small d-block">
							<p>
								D'après les statistiques de google play et analytics itunes{" "}
							</p>
						</span>
					</div>
				</CardTitle>
				<div className="dashboard-donut-chart">
					<DoughnutChart shadow data={deviseUsageChartData} />
				</div>
			</CardBody>
		</Card>
	);
};

export default ProductCategoriesDoughnut;
