import React from "react";
import { Button } from "reactstrap";

//Firebase
import { signInWithGoogle } from "api/firebaseAuth";

//Toast
import { toast } from "react-toastify";

//Custom components

class GoogleAuth extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false
		};
	}
	/**
	 * Login avec Google via Firebase
	 * TODO:
	 *   1. formater les messages d'erreur
	 *   2. Récupérer le numéro de téléphone
	 */
	_googleFirebaseAuth = () => {
		//1.Activer le loader
		this.setState({ isLoading: true }, () => {
			signInWithGoogle(this.props.profileType)
				.then(response => {
					this.setState({ isLoading: false });
					//Récupérer les donnés de l'utilisateur et le token
					const {
						token,
						user: { displayName, email, phoneNumber = "", photoURL, uid }
					} = response;
					/*
						Get the first and last name => faire attention si 
						le nom récupéré contient plus que 2 espaces
					*/
					const [firstName, LastName = null] = displayName.split(" ");
					this.props.getUserInformation({
						email,
						displayName,
						firstName,
						LastName,
						photoUrl: photoURL,
						phoneNumber,
						provider: { name: "google", token, uid }
					});
				})
				.catch(error =>
					this.setState({ isLoading: false }, () =>
						toast.error(error.errorMessage)
					)
				);
		});
	};
	render() {
		const { isLoading } = this.state;
		const { textButtonAction = "Inscription avec Google" } = this.props;
		return (
			<Button
				color="light"
				outline
				disabled={isLoading}
				className={"btn-lg mr-4 px-4 d-flex gcolor"}
				type="button"
				onClick={this._googleFirebaseAuth}
			>
				<span
					className={"glyph-icon pr-2 simple-icon-social-google"}
					style={{ fontSize: "1.2rem", lineHeight: "1.4rem" }}
				/>
				{isLoading ? "Chargement..." : textButtonAction}
			</Button>
		);
	}
}

export default GoogleAuth;

/**
* Exemple comment utiliser le compostant:
* <GoogleAuth
	  profileType={"Customers"}
		language="fr"
		textButtonAction="Inscription avec Google"
		getUserInformation={this.getUserInformationFromGoogle}
	/>
 */
