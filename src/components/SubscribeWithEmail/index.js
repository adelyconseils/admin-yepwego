import React from "react";
//
import { Link } from "react-router-dom";
import {
	Row,
	FormGroup,
	Label,
	Input,
	FormFeedback,
	CustomInput
} from "reactstrap";

import { Colxx } from "components/CustomBootstrap";

//HELPERS
import { handleInputError, isValidEmail } from "util/Validation";
//Api
import { checkIfUserIsExist } from "api/auth";
//Toast
import { toast } from "react-toastify";

//Custom components

class SubscribeWithEmail extends React.Component {
	constructor(props) {
		super(props);
		this.INITIAL_STATE = {
			isLoading: false,
			genderId: "",
			firstName: "",
			LastName: "",
			displayName: "",
			email: "",
			// object erreurs formulaire
			errors: {},
			isValide: true
		};
		this.state = this.INITIAL_STATE;
	}

	//Set the state of form elements
	handelFormElement = event =>
		this.setState({ [event.target.name]: event.target.value });

	/**
	 * Action cliquer sur le bouton action
	 */
	handleActionSubmitForm = () => {
		return new Promise((resolve, reject) => {
			if (!this.isValidForm()) {
				reject(false);
			}
			//Send information Api
			this.setState({ isLoading: true, isValide: true }, () =>
				this._handleRegister()
					.then(response => resolve(response))
					.catch(error => reject(error))
			);
		});
	};
	/**
	 * Vérifier la validité de la formulaire
	 */
	isValidForm = () => {
		//Récupérer les states
		const { firstName, LastName, email, genderId = "" } = this.state;

		//Initialiser l'object erreur
		let errors = {};

		//Vérifier les champs
		if (firstName.length < 2) {
			errors.firstName = { message: "Votre nom est invalide" };
		}
		if (LastName.length < 2) {
			errors.LastName = { message: "Votre prénom est invalide" };
		}
		if (!isValidEmail(email)) {
			errors.email = { message: "Votre E-mail est invalide" };
		}
		if (genderId.length < 1) {
			errors.genderId = { message: "Veuillez sélectionner votre genre" };
		}
		if (Object.entries(errors).length !== 0) {
			return this.setState({ errors }, () => false);
		}
		return true;
	};

	/**
	 * Vérifier si l'utilisateur est déja inscrit
	 */
	_verificationExistUser = (email, profileType) =>
		new Promise((resolve, reject) => {
			checkIfUserIsExist(email, profileType)
				.then(response => resolve(response))
				.catch(error => reject(error));
		});

	_handleRegister = () => {
		return new Promise((resolve, reject) => {
			const { firstName, LastName, displayName, email, genderId } = this.state;
			const {
				profileType,
				messages: { whenUserExist }
			} = this.props;
			this._verificationExistUser(email, profileType)
				.then(response => {
					if (response) {
						toast.error(whenUserExist);
						this.setState({ isValide: false });
						reject({ error: true, message: "USER_EXIST", log: response });
					}
					this.setState({ isLoading: false, isValide: true });
					//send user data
					resolve({
						firstName,
						LastName,
						displayName,
						email,
						genderId
					});
				})
				.catch(error => {
					console.log(error);
					toast.error("Une erreur est survenue. veuillez réessayer plus tard");
					this.setState({ isLoading: false, isValide: false });
					reject(error);
				});
		});
	};

	componentDidMount() {
		if (
			this.props.preloadUserData &&
			Object.keys(this.props.preloadUserData).length
		) {
			const {
				firstName = "",
				LastName = "",
				displayName = "",
				email = "",
				genderId = ""
			} = this.props.preloadUserData;
			this.setState({
				firstName,
				LastName,
				displayName,
				email,
				genderId
			});
		}
	}
	componentDidUpdate(prevProps) {
		if (this.props.preloadUserData !== prevProps.preloadUserData) {
			const {
				firstName,
				LastName,
				displayName,
				email,
				genderId
			} = this.props.preloadUserData;
			this.setState({
				firstName,
				LastName,
				displayName,
				email,
				genderId
			});
		}
	}

	render() {
		const {
			genderId,
			firstName,
			LastName,
			displayName,
			email,
			errors,
			isValide
		} = this.state;

		const {
			profileType,
			config: { gender }
		} = this.props;
		return (
			<>
				{!isValide && (
					<div
						className="alert alert-danger text-center fade show"
						role="alert"
					>
						<span>
							Cette adresse email est déjà utilisée, vous pouvez vous connecter
							en
							<Link to={"/connexion/" + profileType}> cliquant ici.</Link>
						</span>
					</div>
				)}
				<Row>
					<Colxx>
						<FormGroup>
							<div>
								<Label for="email" className="mr-4 mb-0">
									Genre
								</Label>
								{gender.map(gender => (
									<CustomInput
										key={gender.id}
										type="radio"
										id={gender.id + "_" + profileType}
										checked={gender.id === genderId ? true : false}
										name={"genderId"}
										label={gender.name}
										value={gender.id}
										invalid={handleInputError(errors, "genderId")}
										inline
										onChange={this.handelFormElement}
									/>
								))}
							</div>
							<FormFeedback>
								{errors.genderId && errors.genderId.message}
							</FormFeedback>
						</FormGroup>
					</Colxx>
				</Row>
				<Row>
					<Colxx xxs="12" md="6">
						<FormGroup>
							<Label for="firstName">Nom</Label>
							<Input
								type="text"
								name="firstName"
								id="firstName"
								placeholder="Nom"
								value={firstName}
								invalid={handleInputError(errors, "firstName")}
								onChange={this.handelFormElement}
							/>
							<FormFeedback>
								{errors.firstName && errors.firstName.message}
							</FormFeedback>
						</FormGroup>
					</Colxx>
					<Colxx xxs="12" md="6">
						<FormGroup>
							<Label for="LastName">Prénom</Label>
							<Input
								type="text"
								name="LastName"
								placeholder="Prénom"
								value={LastName}
								invalid={handleInputError(errors, "LastName")}
								onChange={this.handelFormElement}
							/>
							<FormFeedback>
								{errors.LastName && errors.LastName.message}
							</FormFeedback>
						</FormGroup>
					</Colxx>
				</Row>
				<FormGroup className="d-none">
					<Label for="displayName">Nom d'utilisateur</Label>
					<Input
						type="text"
						name="displayName"
						placeholder="Nom d'utilisateur"
						value={displayName}
						invalid={handleInputError(errors, "displayName")}
						onChange={this.handelFormElement}
					/>
					<FormFeedback>
						{errors.displayName && errors.displayName.message}
					</FormFeedback>
				</FormGroup>
				<FormGroup>
					<Label for="email">Email</Label>
					<Input
						type="email"
						name="email"
						placeholder="Email"
						value={email}
						invalid={handleInputError(errors, "email")}
						onChange={this.handelFormElement}
					/>
					<FormFeedback>{errors.email && errors.email.message}</FormFeedback>
				</FormGroup>
			</>
		);
	}
}

export default SubscribeWithEmail;

/**
* Exemple comment utiliser le compostant:
* <SubscribeWithEmail
		profileType={"Customers"}
		preloadUserData="{email:'dan@host.com'}"
	/>
 */
