import React, { Component } from "react";
import { Card, CardBody, CardTitle, Button, CardSubtitle } from "reactstrap";
import NotificationManager from "./NotificationManager";

const AlertNotifications = (type, className, title, message) => {
	let cName = className || "";
	return () => {
		switch (type) {
			case "primary":
				NotificationManager.primary(message, title, 3000, null, null, cName);
				break;
			case "secondary":
				NotificationManager.secondary(message, title, 3000, null, null, cName);
				break;
			case "info":
				NotificationManager.info("Info message", "", 3000, null, null, cName);
				break;
			case "success":
				NotificationManager.success(message, title, 3000, null, null, cName);
				break;
			case "warning":
				NotificationManager.warning(message, title, 3000, null, null, cName);
				break;
			case "error":
				NotificationManager.error(
					message,
					title,
					5000,
					() => {
						alert("callback");
					},
					null,
					cName
				);
				break;
			default:
				NotificationManager.info(message);
				break;
		}
	};
};
export default AlertNotifications;
