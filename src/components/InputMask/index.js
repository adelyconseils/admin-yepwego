import React from "react";
//ux
import { Input } from "reactstrap";
import InputMask from "react-input-mask";
const InputMaskHoc = props => (
	<InputMask
		mask={props.mask}
		value={props.value}
		placeholder={props.placeholder}
		onChange={props.onChange}
		invalid={props.invalid}
		type={props.type}
		name={props.name}
	>
		{inputProps => {
			return <Input {...inputProps} disableUnderline />;
		}}
	</InputMask>
);

export default InputMaskHoc;
