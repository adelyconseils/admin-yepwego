import React from "react";
const OrDriver = props => (
	<div className="row full-width mx-2 mt-4">
		<div className="col-md-10 offset-md-1">
			<div className="row">
				<div className="col-5 pr-0">
					<hr />
				</div>
				<div className="col-2 text-center pr-0 pl-0">Où</div>
				<div className="col-5 pl-0">
					<hr />
				</div>
			</div>
		</div>
	</div>
);

export default OrDriver;
