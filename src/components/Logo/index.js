import React from "react";
import { Link } from "react-router-dom";
const Logo = props => (
	<Link to="/">
		<img
			src={process.env.PUBLIC_URL + "/img/YepWeGoIcon.png"}
			style={{
				width: "162px"
			}}
			alt=""
		/>
	</Link>
);

export default Logo;
