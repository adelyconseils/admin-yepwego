import React from "react";
import { Card, CardBody, CardTitle, CardHeader } from "reactstrap";
import {
	buildStyles,
	CircularProgressbarWithChildren
} from "react-circular-progressbar";
let rootStyle = getComputedStyle(document.body);

const themeColor1 = rootStyle.getPropertyValue("--theme-color-2").trim();
const RadialProgressCard = ({ title, percent, isSortable, total }) => {
	return (
		<Card>
			{isSortable && (
				<CardHeader className="p-0 position-relative">
					<div className="position-absolute handle card-icon">
						<i className="simple-icon-shuffle" />
					</div>
				</CardHeader>
			)}
			<CardBody className="d-flex justify-content-between align-items-center">
				<CardTitle className="mb-0">
					{title}:{" " + percent}
				</CardTitle>
				<div className="progress-bar-circle text-center">
					<CircularProgressbarWithChildren
						value={percent}
						styles={buildStyles({
							// Colors
							//rotation: 0.5 + (1 - percent / 100) / 2,
							pathColor: `${themeColor1}`,
							trailColor: "#d6d6d6",
							backgroundColor: "#3e98c7"
						})}
					>
						<div style={{ fontSize: 13 }}>
							<span>{`${Math.round((percent * 100) / total)}%`}</span>
						</div>
					</CircularProgressbarWithChildren>
				</div>
			</CardBody>
		</Card>
	);
};
export default RadialProgressCard;
