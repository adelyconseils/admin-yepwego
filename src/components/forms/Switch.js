import React, { Component } from "react";
import Switch from "rc-switch";
const SwitchElement = props => {
	return (
		<Switch
			className="custom-switch custom-switch-secondary"
			checked={props.value}
			onChange={props.handleFormElement}
			onClick={props.handleFormElement}
			disabled={props.disabled}
			name={props.name}
		/>
	);
};

export default SwitchElement;
